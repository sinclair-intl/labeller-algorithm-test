﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static HeadAllocationSwimLane.TrayRecord;

namespace HeadAllocationSwimLane
{
    class Initialisation
    {

        public List<string> runTest()
        {
            List<string> report = new List<string>();
            fruitImage(ref report);

            //foreach(string item in report)
            //{
            //MessageBox.Show("RunTest section\n" + item);

            //}

            return report;
        }


        //get tray details
        public List<fruitPosition> fruitImage(ref List<string> report)
        {
            string filePath = "";
            //string report = "";
            int fruitTargets = 0;
            TrayRecord.PositionType pt = TrayRecord.PositionType.Complex;
            List<fruitPosition> fruitPositions = new List<fruitPosition>();
            LabellerAllocation labellerAllocation = new LabellerAllocation();

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                //openFileDialog.InitialDirectory = @"D:\Projects\Head allocation algorithm";
                openFileDialog.InitialDirectory = @"D:\Projects\Polski issues\SmartInspectTraylayout\SmartInspectTraylayout\bin\Debug";

                if (openFileDialog.ShowDialog() == DialogResult.OK) 
                {
                    filePath = openFileDialog.FileName;

                    List<string> lines = File.ReadAllLines(filePath).ToList();
                    foreach (string line in lines)
                    {
                        TrayRecord.trayLayout tl = createTrayLayoutOriginal(createTrayLayout(line)) ;
                        labellerAllocation.allocateSearch(ref tl, 1, ref report, pt, ref fruitTargets, false);
                        //MessageBox.Show(report);
                    }
                }
            }

            return fruitPositions;
        }

        public List<fruitPosition> createTrayLayout(string line)
        {
            //List<string> lines = File.ReadAllLines(filePath).ToList();
            string[] strElements = line.Split(',');
            List<fruitPosition> fruitPositions = new List<fruitPosition>();

            foreach (string cells in strElements)
            {
                string[] coordinates = cells.Split(';');
                if(cells == "")
                {
                    continue;
                }

                fruitPositions.Add(new fruitPosition()
                {
                    x = Convert.ToInt32(coordinates[0]),
                    y = Convert.ToInt32(coordinates[1]),
                    selected = false,
                    masked = false,
                    posType = PositionType.Complex

                });
            }
            return fruitPositions;
        }

        public TrayRecord.trayLayout createTrayLayoutOriginal(List<fruitPosition> fruitImage)
        //List<TrayRecord.trayLayout> createTrayLayout()
        {
            List<TrayRecord.trayLayout> trayLayoutList = new List<TrayRecord.trayLayout>();

            TrayRecord.trayLayout tl = new TrayRecord.trayLayout();

            tl.id = 0;
            tl.length = 426;
            tl.width = 258;
            tl.frameHeight = 99;
            tl.firstCol = 0;
            tl.lastCol = 0;
            tl.colCount = 0;
            tl.firstRow = 0;
            tl.lastRow = 0;
            tl.rowCount = 0;

            tl.fruitsReal = fruitImage;

            tl.imageWidth = 0;
            tl.imageHeight = 0;
            tl.imageScaling = 0;
            tl.trayType = PositionType.Complex;
            tl.evenColumnCount = true;
            tl.boxId = 0;
            tl.ghostTrayX = 0;
            tl.ghostTrayY = 0;
            tl.ghostTrayLength = 0;
            tl.ghostTrayWidth = 0;
            tl.vrsMode = true;
            tl.recursionCount = 1;
            tl.reducedColumnCount = 0;


            return tl;
        }

        public void armParameterConfig()
        {
            string filePath = @"armParam.ini";

            List<string> lines = File.ReadAllLines(filePath).ToList();

            foreach (string line in lines)
            {
                //getConfigValues(line);

            }
        }
    }
}

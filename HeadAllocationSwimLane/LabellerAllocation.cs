
namespace HeadAllocationSwimLane
{
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    public class LabellerAllocation
    {
        private static readonly Logger logger = LogManager.GetLogger("LabellerAllocation");
        string debugString = "";

        public enum allocErrorCode { NoError, ColumnsBeyondHeads, AllPathsSearched, PassRequestEqualsZero, InsufficientPasses, SanityCheckFail, LoopIteratorBreak, InsufficientPatternMemory, PatternNotInDatabase, vrsColumnsReduced };
        public enum recursionResult { AllocationComplete, NoResult };
        public enum productAlign { Left, Right, Centre };

        public struct ArmAllocatorParams
        {
            public int armCount;
            public int[] headCounts;
            public int headCentreMin;
            public int headCentreMax;
            public int headFirstMax;
            public int columnBandWidth;
            public int rowBandWidth;
            public int armLength;
            public bool reverseLabelling;
            public bool applicatorDatumLeft;
            public productAlign productAlignment;
            public int datumOffset;
            public int[] armDeltas;
            public float RevBeltSpeed;
            public int reversingOvershoot;
        }

        public struct headReference
        {
            public int pass;
            public int arm;
            public int head;
            public int col;
            public bool tested;
        }

        private struct iterationRecord
        {
            public int headPosPass;
            public int headPosArm;
            public int headPosHead;
            public int allPossIdx;
            public int column;
            public Boolean lastBranch;
        }

        public struct fruitAllocation
        {
            public int trayX;
            public int trayY;
            public int conveyorX;
            public int conveyorY;
            public int colBand;
            public int rowBand;
            public bool colAllocated;
            public bool rowAllocated;
            public headReference finalAllocation;
        }

        private struct colBandAllocation
        {
            public bool allocated;
            public int band;
            public int pass;
            public int arm;
            public int head;
            public List<headReference> possibles;
            public List<int> fruitPositions;
        }

        public enum headStatus { unused, parked, allocated };

        public struct headInfo
        {
            public int actual;
            public int min;
            public int max;
            public headStatus used;
        }

        enum RowCol { Row, Col };

        private List<fruitAllocation> faList;
        private List<int> rowBands;
        private List<colBandAllocation> colAllocation;
        private headInfo[,,] headPosns;
        public ArmAllocatorParams armParams;

        public List<fruitAllocation> allocationResult
        {
            get { return faList; }
        }

        public headInfo[,,] headLocations
        {
            get { return headPosns; }
        }

        List<List<colBandAllocation>> colAllocRecursive;
        List<headInfo[,,]> headPosnsRecursive;

        int[] times = new int[3];

        public LabellerAllocation()
        {
            //LoadAllocationParameters();
            string filePath = @"armParam.ini";

            List<string> lines = File.ReadAllLines(filePath).ToList();

            armParams = new LabellerAllocation.ArmAllocatorParams();

            foreach (string line in lines)
            {
                CreateMachineConst(line);
                //MessageBox.Show(line);
            }

        }

        // Antonio: Added for the Polskie fix
        //######################################################################################
        int incrementCounter = 0;
        int travelTolerance = 20;
        public string strFruitPositionString = "";
        int laneWidth = 0;
        int[,] headPosition = { { 34, 102, 170, 238, 306 }, { 34, 102, 170, 238, 306 }, { 34, 102, 170, 238, 306 }, { 34, 102, 170, 238, 306 } };

        private colLaneAllocation[,] colLaneAllocationList;
        private struct colLaneAllocation
        {
            public bool allocated;
            public int band;
        }

        public void SwimLane()
        {
            int numberHeads = 0;
            for (int headCountIdx = 0; headCountIdx < armParams.armCount; headCountIdx++)
            {
                numberHeads += armParams.headCounts[headCountIdx];
            }

            laneWidth = (armParams.armLength - armParams.headCentreMin) / numberHeads;
        }

        public bool ColumnBandsToLanes()
        {
            try
            {
                int maxHead = getMaxHeadCount();
                colLaneAllocationList = new colLaneAllocation[armParams.armCount, maxHead];

                SwimLane();
                //first split into nearset bands to lanes
                for (int IDcounter = 0; IDcounter < colAllocation.Count; IDcounter++)
                {
                    int columnPosition = colAllocation[IDcounter].band;
                    int head = (columnPosition - (armParams.headCentreMin / 2)) / (armParams.armCount * laneWidth);

                    if (head > maxHead - 1)
                    {
                        head = maxHead - 1;
                    }

                    if (columnPosition + armParams.headCentreMin - (armParams.armCount * laneWidth * head) > laneWidth)
                    {
                        if (head == 0)
                        {
                            if (!AssignFirstLane(1, head, IDcounter))
                            {
                                return false;
                            }

                        }
                        else
                        {
                            if (!AssignLane(1, head, IDcounter))
                            {
                                return false;
                            }

                        }
                    }
                    else
                    {
                        if (head == 0)
                        {
                            if (!AssignFirstLane(0, head, IDcounter))
                            {
                                return false;
                            }

                        }
                        else
                        {
                            if (!AssignLane(0, head, IDcounter))
                            {
                                return false;
                            }

                        }
                    }
                }

                //return true;
                return MaxPositionCheck();
            }
            catch (Exception ex)
            {
                logger.Debug(ex.Message);
                return false;
            }
        }

        public bool AssignLane(int arm, int head, int columnID)
        {
            int maxHead = armParams.headCounts[arm] - 1;
            int armOpposite = 0;

            if (arm != 1)
            {
                armOpposite = 1;
            }

            if (colLaneAllocationList[arm, head].allocated == false &&
                    (colAllocation[columnID].band - colLaneAllocationList[arm, head - 1].band >= armParams.headCentreMin) &&
                    (head <= armParams.headCounts[arm] - 1))
            {
                return ApplicatorSpacingCheck(arm, head, columnID);
            }
            //else if (head < maxHead)
            else if (head < armParams.headCounts[armOpposite] - 1)
            {
                if (colLaneAllocationList[armOpposite, head].allocated == false &&
                    (colAllocation[columnID].band - colLaneAllocationList[armOpposite, head - 1].band >= armParams.headCentreMin))
                {
                    return ApplicatorSpacingCheck(armOpposite, head, columnID);
                }
                else
                {
                    if (colLaneAllocationList[armOpposite, head + 1].allocated == false &&
                        (colAllocation[columnID].band - colLaneAllocationList[armOpposite, head].band >= armParams.headCentreMin))
                    {
                        return ApplicatorSpacingCheck(armOpposite, head + 1, columnID);
                    }
                    else if (colLaneAllocationList[arm, head + 1].allocated == false &&
                        (colAllocation[columnID].band - colLaneAllocationList[arm, head].band >= armParams.headCentreMin) &&
                        head < armParams.headCounts[arm] - 1)
                    {
                        return ApplicatorSpacingCheck(arm, head + 1, columnID);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else if (head < maxHead) //If it doesn't make it through previous section due to less heads on opposite arm
            {
                if (colLaneAllocationList[arm, head + 1].allocated == false &&
                        (colAllocation[columnID].band - colLaneAllocationList[arm, head].band >= armParams.headCentreMin))
                {
                    return ApplicatorSpacingCheck(arm, head + 1, columnID);
                }
                else
                {
                    return false;
                }

            }
            else if ((colLaneAllocationList[armOpposite, head].allocated == false &&
                (colAllocation[columnID].band - colLaneAllocationList[armOpposite, head - 1].band >= armParams.headCentreMin)))
            {
                return ApplicatorSpacingCheck(armOpposite, head, columnID);
            }
            else
            {
                if (colAllocation[columnID].band - colLaneAllocationList[arm, head].band >= armParams.headCentreMin)
                {
                    return ShuffleDown(columnID, arm);
                }
                else if (colAllocation[columnID].band - colLaneAllocationList[armOpposite, head].band >= armParams.headCentreMin)
                {
                    return ShuffleDown(columnID, armOpposite);
                }
                else
                {
                    return false;
                }
            }
        }

        public bool AssignFirstLane(int arm, int head, int columnID)
        {
            int maxHead = armParams.headCounts[arm] - 1;
            int armOpposite = 0;

            if (arm != 1)
            {
                armOpposite = 1;
            }

            if (colLaneAllocationList[arm, head].allocated == false)
            {
                colLaneAllocationList[arm, head].allocated = true;
                colLaneAllocationList[arm, head].band = colAllocation[columnID].band;
            }
            //else if (head < maxHead)
            else if (head < armParams.headCounts[armOpposite] - 1)
            {
                if (colLaneAllocationList[armOpposite, head].allocated == false)
                {
                    colLaneAllocationList[armOpposite, head].allocated = true;
                    colLaneAllocationList[armOpposite, head].band = colAllocation[columnID].band;
                }
                else
                {
                    if (colLaneAllocationList[armOpposite, head + 1].allocated == false &&
                        (colAllocation[columnID].band - colLaneAllocationList[armOpposite, head].band >= armParams.headCentreMin))
                    {
                        colLaneAllocationList[armOpposite, head + 1].allocated = true;
                        colLaneAllocationList[armOpposite, head + 1].band = colAllocation[columnID].band;
                    }
                    else if (colLaneAllocationList[arm, head + 1].allocated == false &&
                        (colAllocation[columnID].band - colLaneAllocationList[arm, head].band > armParams.headCentreMin))
                    {
                        colLaneAllocationList[arm, head + 1].allocated = true;
                        colLaneAllocationList[arm, head + 1].band = colAllocation[columnID].band;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else if (head < maxHead) //If it doesn't make it through previous section due to less heads on opposite arm
            {
                if (colLaneAllocationList[arm, head + 1].allocated == false &&
                        (colAllocation[columnID].band - colLaneAllocationList[arm, head].band > armParams.headCentreMin))
                {
                    colLaneAllocationList[arm, head + 1].allocated = true;
                    colLaneAllocationList[arm, head + 1].band = colAllocation[columnID].band;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                //Need to shuffle down
                if (colAllocation[columnID].band - colLaneAllocationList[arm, head].band >= armParams.headCentreMin)
                {
                    return ShuffleDown(columnID, arm);
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public bool ApplicatorSpacingCheck(int arm, int headCheck, int columnID)
        {
            int gapFactor = 1;
            int minimumGap = 0;
            int maximumGap = 0;

            for (int head = headCheck - 1; head >= 0; head--)
            {
                if (colLaneAllocationList[arm, head].allocated == true)
                {
                    break;
                }
                else if (colLaneAllocationList[arm, head].allocated == false &&
                    head == 0)
                {
                    gapFactor = 0;   //we can shift the row down later and there's no other applicator to check against
                }
                else
                {
                    gapFactor++;
                }
            }

            //minimumGap = armParams.headCentreMin * gapFactor;
            minimumGap = (armParams.headCentreMin + 2) * gapFactor;
            maximumGap = armParams.headCentreMax * gapFactor;

            if (gapFactor != 0)
            {
                int gap = colAllocation[columnID].band - colLaneAllocationList[arm, headCheck - gapFactor].band;

                if (gap >= minimumGap &&
                    gap <= maximumGap)
                {
                    colLaneAllocationList[arm, headCheck].allocated = true;
                    colLaneAllocationList[arm, headCheck].band = colAllocation[columnID].band;
                    return true;
                }
                else if (gap > maximumGap)
                {
                    if (headCheck == armParams.headCounts[arm] - 1)
                    {
                        if (colAllocation[columnID].band - colLaneAllocationList[arm, headCheck - 1].band >= minimumGap)
                        {
                            return ShuffleDown(columnID, arm);
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (colLaneAllocationList[arm, headCheck + 1].allocated == false)
                        {
                            return ApplicatorSpacingCheck(arm, headCheck + 1, columnID);
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    if (colLaneAllocationList[arm, headCheck - 1].allocated == false)
                    {
                        return ApplicatorSpacingCheck(arm, headCheck - 1, columnID);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                colLaneAllocationList[arm, headCheck].allocated = true;
                colLaneAllocationList[arm, headCheck].band = colAllocation[columnID].band;
                return true;
            }
        }

        public bool ShuffleDown(int colPosition, int arm)
        {
            for (int head = armParams.headCounts[arm] - 1; head >= 0; head--)
            {
                //if theres no opening on this arm we need to flip to other arm
                if (colLaneAllocationList[arm, head].allocated == false && head != armParams.headCounts[arm] - 1)
                {
                    if (head > 0)
                    {
                        if (colLaneAllocationList[arm, head + 1].band - colLaneAllocationList[arm, head - 1].band >= armParams.headCentreMin &&
                            colLaneAllocationList[arm, head + 1].band - colLaneAllocationList[arm, head - 1].band <= armParams.headCentreMax)
                        {
                            //check that theres enough space between heads
                            int headCounter = 1;
                            for (int headCheck = head - 1; headCheck >= 0; headCheck--)
                            {
                                if (colLaneAllocationList[arm, head + 1].band - colLaneAllocationList[arm, head - headCounter].band < armParams.headCentreMin * (headCounter + 1) &&
                                    colLaneAllocationList[arm, head - headCounter].allocated != false)
                                {
                                    return false;
                                }
                                headCounter++;
                            }
                            //set next column back 1
                            MoveColumnsDown(arm, head, colPosition);
                            break;
                        }
                    }
                    else
                    {
                        MoveColumnsDown(arm, head, colPosition);
                        break;
                    }
                }
                else if (colLaneAllocationList[arm, head].allocated == true && head == 0)
                {
                    return false;
                }
            }
            return true;
        }

        public bool ShuffleUp(int arm)
        {
            for (int head = armParams.headCounts[arm] - 1; head >= 0; head--)
            {
                if (colLaneAllocationList[arm, head].allocated == false && head != armParams.headCounts[arm] - 1)
                {
                    if (head > 0)
                    {
                        if (colLaneAllocationList[arm, head + 1].band - colLaneAllocationList[arm, head - 1].band >= armParams.headCentreMin &&
                            colLaneAllocationList[arm, head + 1].band - colLaneAllocationList[arm, head - 1].band <= armParams.headCentreMax)
                        {
                            //check that theres enough space between heads
                            int headCounter = 1;
                            for (int headCheck = head - 1; headCheck >= 0; headCheck--)
                            {
                                if (colLaneAllocationList[arm, head + 1].band - colLaneAllocationList[arm, head - headCounter].band < armParams.headCentreMin * (headCounter + 1) &&
                                    colLaneAllocationList[arm, head - headCounter].allocated != false)
                                {
                                    return false;
                                }
                                headCounter++;
                            }

                            //set next column back 1
                            MoveColumnsUp(arm);
                            break;
                        }
                    }
                    else
                    {
                        MoveColumnsUp(arm);
                        break;
                    }
                }
                else if (colLaneAllocationList[arm, head].allocated == true && head == 0)
                {
                    return false;
                }
            }
            return true;
        }


        public void MoveColumnsDown(int arm, int headStart, int colPosition)
        {
            int maxHead = armParams.headCounts[arm] - 1;

            for (int head = headStart; head <= maxHead - 1; head++)
            {
                //set new head position to one up
                colLaneAllocationList[arm, head].allocated = colLaneAllocationList[arm, head + 1].allocated;
                colLaneAllocationList[arm, head].band = colLaneAllocationList[arm, head + 1].band;

                //set old head position to unassigned values
                colLaneAllocationList[arm, head + 1].allocated = false;
                colLaneAllocationList[arm, head + 1].band = 0;

            }
            colLaneAllocationList[arm, maxHead].allocated = true;
            colLaneAllocationList[arm, maxHead].band = colAllocation[colPosition].band;
        }
        public void MoveColumnsDown(int arm, int headStart)
        {
            int maxHead = armParams.headCounts[arm] - 1;

            for (int head = 0; head <= maxHead - 1; head++)
            {
                //set new head position to one up
                colLaneAllocationList[arm, head].allocated = colLaneAllocationList[arm, head + 1].allocated;
                colLaneAllocationList[arm, head].band = colLaneAllocationList[arm, head + 1].band;

                //set old head position to unassigned values
                colLaneAllocationList[arm, head + 1].allocated = false;
                colLaneAllocationList[arm, head + 1].band = 0;
            }
        }

        public bool MoveColumnsUp(int arm)
        {
            int maxHead = armParams.headCounts[arm] - 1;


            for (int head = maxHead; head > 0; head--)
            {
                colLaneAllocationList[arm, head].allocated = colLaneAllocationList[arm, head - 1].allocated;
                colLaneAllocationList[arm, head].band = colLaneAllocationList[arm, head - 1].band;

                colLaneAllocationList[arm, head - 1].allocated = false;
                colLaneAllocationList[arm, head - 1].band = 0;


                if (colLaneAllocationList[arm, head].allocated == true)
                {
                    for (int headCheck = head - 1; headCheck > 0; headCheck--)
                    {
                        if (colLaneAllocationList[arm, headCheck + 1].allocated == true &&
                            colLaneAllocationList[arm, headCheck].allocated == false &&
                            colLaneAllocationList[arm, headCheck + 1].band - colLaneAllocationList[arm, headCheck - 1].band >= armParams.headCentreMin * 2 &&
                            colLaneAllocationList[arm, headCheck + 1].band - colLaneAllocationList[arm, headCheck - 1].band <= armParams.headCentreMax * 2)
                        {
                            return true;
                        }
                    }

                    if (head == 1 &&
                        colLaneAllocationList[arm, head].band < armParams.headCentreMin + armParams.headCentreMin / 2)
                    {
                        return false;
                    }
                    else if (head == 1 &&
                        colLaneAllocationList[arm, head].band >= armParams.headCentreMin + armParams.headCentreMin / 2)
                    {
                        return true;
                    }
                }
            }
            return true;
        }

        public bool ArmSwitchComparison(int arm, int armOpposite)
        {
            int datumnGap = armParams.headCentreMin / 2;
            int maxHead = armParams.headCounts[arm] - 1;

            for (int headCheck = 0; headCheck <= maxHead; headCheck++)
            {
                if (colLaneAllocationList[arm, headCheck].allocated == true &&
                    colLaneAllocationList[arm, headCheck].band < datumnGap)
                {
                    int oppositeDatumnGap = armParams.headCentreMin / 2;

                    for (int oppositHeadCheck = 0; oppositHeadCheck <= maxHead; oppositHeadCheck++)
                    {
                        if (colLaneAllocationList[armOpposite, oppositHeadCheck].allocated == true &&
                    colLaneAllocationList[armOpposite, oppositHeadCheck].band >= oppositeDatumnGap)
                        {
                            if (oppositHeadCheck > 0)
                            {
                                if (colLaneAllocationList[armOpposite, oppositHeadCheck].band - armParams.headCentreMin > colLaneAllocationList[arm, headCheck].band)// have the issue of the first and second apps being too close to datumn point
                                {
                                    if (colLaneAllocationList[armOpposite, oppositHeadCheck - 1].allocated == false)
                                    {
                                        colLaneAllocationList[armOpposite, oppositHeadCheck - 1].band = colLaneAllocationList[arm, headCheck].band;
                                        colLaneAllocationList[armOpposite, oppositHeadCheck - 1].allocated = true;

                                        colLaneAllocationList[arm, headCheck].allocated = false;
                                        colLaneAllocationList[arm, headCheck].band = 0;
                                        return true;
                                    }
                                }
                                else
                                {
                                    for (int headLoop = 0; headLoop <= maxHead - headCheck; headLoop++)
                                    {
                                        int oppositeGapCheck;
                                        int oppositeSelector = 0;

                                        if (headLoop == 0 && oppositHeadCheck == 0)
                                        {
                                            oppositeGapCheck = MinBandCheckUpwards(armOpposite, oppositHeadCheck);
                                        }
                                        else
                                        {
                                            oppositeGapCheck = MinBandCheckUpwards(armOpposite, oppositHeadCheck + (headLoop - 1));
                                            oppositeSelector = (headLoop - 1);
                                        }

                                        int armGapCheck = MinBandCheckUpwards(arm, headCheck + headLoop);

                                        if (oppositeGapCheck > colLaneAllocationList[arm, headCheck + headLoop].band &&
                                            armGapCheck > colLaneAllocationList[armOpposite, oppositHeadCheck + oppositeSelector].band)
                                        {
                                            for (int headSetLoop = 0; headSetLoop < headCheck + headLoop; headSetLoop--)
                                            {
                                                int tempBand = 0;
                                                bool tempAlloc;
                                                tempBand = colLaneAllocationList[armOpposite, headSetLoop].band;
                                                tempAlloc = colLaneAllocationList[armOpposite, headSetLoop].allocated;

                                                colLaneAllocationList[armOpposite, headSetLoop].band = colLaneAllocationList[arm, headSetLoop + 1].band;
                                                colLaneAllocationList[armOpposite, headSetLoop].allocated = colLaneAllocationList[arm, headSetLoop + 1].allocated;

                                                colLaneAllocationList[arm, headSetLoop + 1].band = tempBand;
                                                colLaneAllocationList[arm, headSetLoop + 1].allocated = tempAlloc;
                                            }
                                            return true;
                                        }

                                        if (headCheck + headLoop != maxHead)
                                        {
                                            oppositeGapCheck = MinBandCheckUpwards(armOpposite, oppositHeadCheck + headLoop);
                                            if (oppositeGapCheck > colLaneAllocationList[arm, headCheck + headLoop].band &&
                                               armGapCheck > colLaneAllocationList[armOpposite, oppositHeadCheck + headLoop].band)
                                            {
                                                for (int headSetLoop = 0; headSetLoop <= headCheck + headLoop; headSetLoop--)
                                                {
                                                    int tempBand = 0;
                                                    bool tempAlloc;
                                                    tempBand = colLaneAllocationList[armOpposite, headSetLoop].band;
                                                    tempAlloc = colLaneAllocationList[armOpposite, headSetLoop].allocated;

                                                    colLaneAllocationList[armOpposite, headSetLoop].band = colLaneAllocationList[arm, headSetLoop].band;
                                                    colLaneAllocationList[armOpposite, headSetLoop].allocated = colLaneAllocationList[arm, headSetLoop].allocated;

                                                    colLaneAllocationList[arm, headSetLoop].band = tempBand;
                                                    colLaneAllocationList[arm, headSetLoop].allocated = tempAlloc;
                                                }
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int headLoop = 0; headLoop <= maxHead - headCheck; headLoop++)
                                {
                                    int oppositeGapCheck;
                                    int oppositeSelector = 0;

                                    if (headLoop == 0 && oppositHeadCheck == 0)
                                    {
                                        oppositeGapCheck = MinBandCheckUpwards(armOpposite, oppositHeadCheck);
                                    }
                                    else
                                    {
                                        oppositeGapCheck = MinBandCheckUpwards(armOpposite, oppositHeadCheck + (headLoop - 1));
                                        oppositeSelector = (headLoop - 1);
                                    }

                                    int armGapCheck = MinBandCheckUpwards(arm, headCheck + headLoop);

                                    if (oppositeGapCheck > colLaneAllocationList[arm, headCheck + headLoop].band &&
                                        armGapCheck > colLaneAllocationList[armOpposite, oppositHeadCheck + oppositeSelector].band)
                                    {
                                        for (int headSetLoop = 0; headSetLoop < headCheck + headLoop; headSetLoop--)
                                        {
                                            int tempBand = 0;
                                            bool tempAlloc;
                                            tempBand = colLaneAllocationList[armOpposite, headSetLoop].band;
                                            tempAlloc = colLaneAllocationList[armOpposite, headSetLoop].allocated;

                                            colLaneAllocationList[armOpposite, headSetLoop].band = colLaneAllocationList[arm, headSetLoop + 1].band;
                                            colLaneAllocationList[armOpposite, headSetLoop].allocated = colLaneAllocationList[arm, headSetLoop + 1].allocated;

                                            colLaneAllocationList[arm, headSetLoop + 1].band = tempBand;
                                            colLaneAllocationList[arm, headSetLoop + 1].allocated = tempAlloc;
                                        }
                                    }

                                    if (headCheck + headLoop != maxHead)
                                    {
                                        oppositeGapCheck = MinBandCheckUpwards(armOpposite, oppositHeadCheck + headLoop);
                                        if (oppositeGapCheck > colLaneAllocationList[arm, headCheck + headLoop].band &&
                                           armGapCheck > colLaneAllocationList[armOpposite, oppositHeadCheck + headLoop].band)
                                        {
                                            for (int headSetLoop = 0; headSetLoop <= headCheck + headLoop; headSetLoop--)
                                            {
                                                int tempBand = 0;
                                                bool tempAlloc;
                                                tempBand = colLaneAllocationList[armOpposite, headSetLoop].band;
                                                tempAlloc = colLaneAllocationList[armOpposite, headSetLoop].allocated;

                                                colLaneAllocationList[armOpposite, headSetLoop].band = colLaneAllocationList[arm, headSetLoop].band;
                                                colLaneAllocationList[armOpposite, headSetLoop].allocated = colLaneAllocationList[arm, headSetLoop].allocated;

                                                colLaneAllocationList[arm, headSetLoop].band = tempBand;
                                                colLaneAllocationList[arm, headSetLoop].allocated = tempAlloc;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        oppositeDatumnGap += armParams.headCentreMin;
                    }
                }
                datumnGap += armParams.headCentreMin;
            }
            return false;
        }

        public int MinBandCheckUpwards(int arm, int head)
        {
            int maxHead = armParams.headCounts[arm] - 1;
            int headBand = colLaneAllocationList[arm, head].band;
            int minGap;

            if (head == 0)
            {
                minGap = armParams.headCentreMin / 2;
            }
            else
            {
                minGap = armParams.headCentreMin;
            }


            for (int headCheck = head + 1; headCheck <= maxHead; headCheck++)
            {
                if (colLaneAllocationList[arm, headCheck].allocated == true)
                {
                    minGap = colLaneAllocationList[arm, headCheck].band - minGap;
                    return minGap;
                }
                minGap += armParams.headCentreMin;
            }

            return armParams.armLength - minGap;
        }
        public void fruitLocations()
        {
            strFruitPositionString = "";

            foreach (fruitAllocation fa in faList)
            {
                strFruitPositionString += fa.trayX + ";" + fa.conveyorY + ",";
            }

        }

        public void DistanceBetweenBands()
        {
            int extraback = 0;
            int previousBand = 0;

            int bandDistance = 0;
            for (int IDcounter = 0; IDcounter < colAllocation.Count; IDcounter++)
            {
                extraback = Math.Abs(previousBand - colAllocation[IDcounter].band) + bandDistance;
                bandDistance = Math.Abs(previousBand - colAllocation[IDcounter].band);
                previousBand = colAllocation[IDcounter].band;
                int minColPosition = armParams.headCentreMin / 2;
                int maxColPosition = armParams.armLength - (int)(armParams.headCentreMin / 2);

                if (colAllocation[IDcounter].band > maxColPosition)
                {
                    removeColumnsNFruit(IDcounter);
                    DistanceBetweenBands();
                    break;
                }

                if (colAllocation[IDcounter].band < minColPosition)
                {
                    removeColumnsNFruit(IDcounter);
                    DistanceBetweenBands();
                    break;
                }


                if (IDcounter == 0)
                {
                    continue;
                }

                if (extraback < armParams.headCentreMin)
                {
                    if (colAllocation[IDcounter - 2].fruitPositions.Count < colAllocation[IDcounter - 1].fruitPositions.Count)
                    {
                        //return true and make copy of old head layout - that way we could cycle through different options
                        if (colAllocation[IDcounter - 2].fruitPositions.Count < colAllocation[IDcounter].fruitPositions.Count)
                        {
                            //colAllocation.RemoveAt(IDcounter - 2);
                            removeColumnsNFruit(IDcounter - 2);
                            DistanceBetweenBands();
                            break;
                        }
                        else
                        {
                            //colAllocation.RemoveAt(IDcounter);
                            removeColumnsNFruit(IDcounter);
                            DistanceBetweenBands();
                            break;
                        }
                    }
                    else
                    {
                        if (colAllocation[IDcounter - 1].fruitPositions.Count < colAllocation[IDcounter].fruitPositions.Count)
                        {
                            //colAllocation.RemoveAt(IDcounter - 1);
                            removeColumnsNFruit(IDcounter - 1);
                            DistanceBetweenBands();
                            break;
                        }
                        else
                        {
                            //colAllocation.RemoveAt(IDcounter);
                            removeColumnsNFruit(IDcounter);
                            DistanceBetweenBands();
                            break;
                        }
                    }
                }
            }
        }

        public void removeColumnsNFruit(int columnNumber)
        {
            int colBand = colAllocation[columnNumber].band;
            colAllocation.RemoveAt(columnNumber);

            List<int> faDeletions = new List<int>();
            for (int faIdx = 0; faIdx < faList.Count; faIdx++)
            {
                if (faList[faIdx].colBand == colBand)
                {
                    faDeletions.Add(faIdx);
                }
            }

            faDeletions.Reverse();
            foreach (int deletefa in faDeletions)
            {
                faList.RemoveAt(deletefa);
            }

        }

        public void removeExtraColumns()
        {
            int maxColCount = 0;
            for (int headCountIdx = 0; headCountIdx < armParams.armCount; headCountIdx++)
            {
                maxColCount += armParams.headCounts[headCountIdx];
            }

            while (colAllocation.Count > maxColCount)
            {
                int columnID = 0;
                int tempMin = 9999;
                for (int IDcounter = 0; IDcounter < colAllocation.Count; IDcounter++)
                {
                    if (colAllocation[IDcounter].fruitPositions.Count < tempMin)
                    {
                        columnID = IDcounter;
                    }
                }
                removeColumnsNFruit(columnID);
            }
        }


        private void updateHeadAllocation(ref headInfo[,,] headPosns)
        {
            int pass;
            int arm;
            int head;

            for (pass = 0; pass < headPosns.GetLength(0); pass++)
            {
                for (arm = 0; arm < headPosns.GetLength(1); arm++)
                {
                    /* Find/fix all of the allocated head positions first */
                    //for (head = 0; head < headPosns.GetLength(2); head++)
                    for (head = 0; head < armParams.headCounts[arm]; head++)
                    {
                        if (colLaneAllocationList[arm, head].allocated == true)
                        {
                            PreviousPositionCheckSet(arm, head);
                            headPosns[pass, arm, head].used = headStatus.allocated;
                            headPosns[pass, arm, head].actual = colLaneAllocationList[arm, head].band;
                            headPosns[pass, arm, head].min = colLaneAllocationList[arm, head].band;
                            headPosns[pass, arm, head].max = colLaneAllocationList[arm, head].band;
                        }
                        else
                        {
                            headPosns[pass, arm, head].used = headStatus.unused;
                            headPosns[pass, arm, head].actual = 0;
                            headPosns[pass, arm, head].min = armParams.headCentreMin / 2;
                            headPosns[pass, arm, head].max = armParams.armLength - armParams.headCentreMin / 2;
                        }
                    }

                    /* Update min/max from allocated heads */
                    for (head = 0; head < armParams.headCounts[arm]; head++)
                    {
                        if (headPosns[pass, arm, head].used == headStatus.allocated)
                        {
                            /* Working down the heads */
                            for (int headNum = head - 1; headNum >= 0; headNum--)
                            {
                                if (headPosns[pass, arm, headNum].used == headStatus.allocated)
                                {
                                    break;
                                }
                                else
                                {
                                    int newMax = headPosns[pass, arm, headNum + 1].max - armParams.headCentreMin;
                                    if (newMax < headPosns[pass, arm, headNum].max)
                                    {
                                        headPosns[pass, arm, headNum].max = newMax;
                                    }

                                    int newMin = headPosns[pass, arm, headNum + 1].min - armParams.headCentreMax;
                                    if (newMin > headPosns[pass, arm, headNum].min)
                                    {
                                        headPosns[pass, arm, headNum].min = newMin;
                                    }
                                }
                            }

                            /* Working up the heads */
                            for (int headNum = head + 1; headNum < armParams.headCounts[arm]; headNum++)
                            {
                                if (headPosns[pass, arm, headNum].used == headStatus.allocated)
                                {
                                    break;
                                }
                                else
                                {
                                    int newMax = headPosns[pass, arm, headNum - 1].max + armParams.headCentreMax;
                                    if (newMax < headPosns[pass, arm, headNum].max)
                                    {
                                        headPosns[pass, arm, headNum].max = newMax;
                                    }

                                    int newMin = headPosns[pass, arm, headNum - 1].min + armParams.headCentreMin;
                                    if (newMin > headPosns[pass, arm, headNum].min)
                                    {
                                        headPosns[pass, arm, headNum].min = newMin;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void PreviousPositionCheckSet(int arm, int head)
        {
            int widthLimiter = armParams.headCentreMin / 2;
            int columnBand = colLaneAllocationList[arm, head].band;
            int previousAllocation = headPosition[arm, head];

            if (colLaneAllocationList[arm, head].allocated &&
                Math.Abs(previousAllocation - columnBand) < 10 &&
                previousAllocation - columnBand != 0)
            {
                int directionMove = previousAllocation - columnBand;

                if (directionMove > 0)
                {
                    int upperGap = armParams.headCentreMin;
                    int upperGapMax = armParams.headCentreMax;
                    for (int headCheck = head + 1; headCheck < armParams.headCounts[arm]; headCheck++)
                    {
                        if (colLaneAllocationList[arm, headCheck].allocated == false)
                        {
                            upperGap += armParams.headCentreMin;
                            continue;
                        }
                        else
                        {
                            if (Math.Abs(previousAllocation - colLaneAllocationList[arm, headCheck].band) < upperGap &&
                            headCheck != head)
                            {
                                previousAllocation = colLaneAllocationList[arm, headCheck].band - upperGap;
                            }
                            break;
                        }

                    }
                    for (int headCheck = head - 1; headCheck >= 0; headCheck--)
                    {
                        if (colLaneAllocationList[arm, headCheck].allocated == false)
                        {
                            upperGapMax += armParams.headCentreMin;
                            continue;
                        }
                        else
                        {
                            if (Math.Abs(previousAllocation - colLaneAllocationList[arm, headCheck].band) > upperGapMax &&
                            headCheck != head)
                            {
                                previousAllocation = colLaneAllocationList[arm, headCheck].band + upperGapMax;
                            }
                            break;
                        }
                    }
                }
                else
                {
                    int lowerGap = armParams.headCentreMin;
                    int lowerGapMax = armParams.headCentreMax;
                    for (int headCheck = head - 1; headCheck >= 0; headCheck--)
                    {
                        if (colLaneAllocationList[arm, headCheck].allocated == false)
                        {
                            lowerGap += armParams.headCentreMin;
                            continue;
                        }
                        else
                        {
                            if (Math.Abs(previousAllocation - colLaneAllocationList[arm, headCheck].band) < lowerGap &&
                            headCheck != head)
                            {
                                previousAllocation = colLaneAllocationList[arm, headCheck].band + lowerGap;
                            }
                            break;
                        }

                    }
                    for (int headCheck = head + 1; headCheck < armParams.headCounts[arm]; headCheck++)
                    {
                        if (colLaneAllocationList[arm, headCheck].allocated == false)
                        {
                            lowerGapMax += armParams.headCentreMin;
                            continue;
                        }
                        else
                        {
                            if (Math.Abs(previousAllocation - colLaneAllocationList[arm, headCheck].band) > lowerGapMax &&
                            headCheck != head)
                            {
                                previousAllocation = colLaneAllocationList[arm, headCheck].band - lowerGapMax;
                            }
                            break;
                        }
                    }
                }
                colLaneAllocationList[arm, head].band = previousAllocation;
            }
        }

        public bool CheckerDefault()
        {
            int maxHead = getMaxHeadCount();
            colLaneAllocationList = new colLaneAllocation[armParams.armCount, maxHead];

            for (int IDcounter = 0; IDcounter < colAllocation.Count; IDcounter++)
            {
                int arm = IDcounter % 2;
                int head = IDcounter / 2;
                if (arm == 1)
                {
                    if (head == 0)
                    {
                        colLaneAllocationList[arm, head].allocated = true;
                        colLaneAllocationList[arm, head].band = colAllocation[IDcounter].band;
                    }
                    else
                    {
                        int prevApplicatorBand = colLaneAllocationList[arm, head - 1].band;
                        int currentApplicatorBand = colAllocation[IDcounter].band;
                        if (currentApplicatorBand - prevApplicatorBand >= armParams.headCentreMin &&
                            currentApplicatorBand - prevApplicatorBand <= armParams.headCentreMax)
                        {
                            colLaneAllocationList[arm, head].allocated = true;
                            colLaneAllocationList[arm, head].band = colAllocation[IDcounter].band;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    if (head == 0)
                    {
                        colLaneAllocationList[arm, head].allocated = true;
                        colLaneAllocationList[arm, head].band = colAllocation[IDcounter].band;
                    }
                    else
                    {
                        int prevApplicatorBand = colLaneAllocationList[arm, head - 1].band;
                        int currentApplicatorBand = colAllocation[IDcounter].band;
                        if (currentApplicatorBand - prevApplicatorBand >= armParams.headCentreMin &&
                            currentApplicatorBand - prevApplicatorBand <= armParams.headCentreMax)
                        {
                            colLaneAllocationList[arm, head].allocated = true;
                            colLaneAllocationList[arm, head].band = colAllocation[IDcounter].band;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public bool MaxPositionCheck()
        {
            for (int pass = 0; pass < headPosns.GetLength(0); pass++)
            {
                for (int arm = 0; arm < headPosns.GetLength(1); arm++)
                {
                    int headCounter = 0;
                    //rep += "\r\nPass " + pass.ToString() + ", Arm " + arm.ToString() + ": ";
                    for (int head = armParams.headCounts[arm] - 1; head >= 0; head--)
                    {
                        if (colLaneAllocationList[arm, head].allocated == true)
                        {
                            if (colLaneAllocationList[arm, head].band > armParams.armLength - (armParams.headCentreMin / 2) - (armParams.headCentreMin * headCounter))
                            {
                                if (!MoveColumnsUp(arm))
                                {
                                    return false;
                                }
                                return MaxPositionCheck();
                            }
                        }
                        headCounter++;

                    }
                }
            }
            return true;
        }

        private void SetNewAllocationToFruit()
        {
            for (int n = 0; n < faList.Count; n++)
            {
                headReference hr = new headReference();
                hr = SearchAllocatedNewColumn(faList[n].colBand);

                fruitAllocation fa = faList[n];
                fa.finalAllocation = hr;

                // Add the deltas dependant on the allocated arm
                fa.conveyorX = fa.rowBand;
                for (int m = 0; m <= hr.arm; m++)
                {
                    fa.conveyorX += armParams.armDeltas[m];
                }
                faList.RemoveAt(n);
                faList.Insert(n, fa);
            }
        }

        private headReference SearchAllocatedNewColumn(int col)
        {
            headReference pah = new headReference();

            for (int pass = 0; pass < headPosns.GetLength(0); pass++)
            {
                for (int arm = 0; arm < headPosns.GetLength(1); arm++)
                {
                    for (int head = 0; head < headPosns.GetLength(2); head++)
                    {
                        if (headPosns[pass, arm, head].used == headStatus.allocated)
                        {
                            if (Math.Abs(col - headPosns[pass, arm, head].actual) < travelTolerance)
                            {
                                pah.pass = pass;
                                pah.arm = arm;
                                pah.head = head;
                                return pah;
                            }
                        }
                    }
                }
            }
            return pah;
        }

        public void TL4AllocationAlgorithm()
        {
            int maxHead = getMaxHeadCount();
            int armNumber = 0;
            int headNumber = 0;
            int columnNumber = 0;

            colLaneAllocationList = new colLaneAllocation[armParams.armCount, maxHead];
            int[,] appAssignColumn = new int[armParams.armCount, maxHead];
            bool[] columnAssigned = new bool[colAllocation.Count];

            int[] intLastAllocatedHead = new int[armParams.armCount];
            for (int arm = 0; arm < armParams.armCount; arm++)
            {
                intLastAllocatedHead[arm] = 0;
            }
            for (int column = 0; column < colAllocation.Count; column++)
            {
                columnAssigned[column] = false;
            }

            do
            {
                if (colAllocation[columnNumber].band < armParams.headFirstMax) //If (gintRowPosition(r) + gintLabelCentreOffset) < (HEAD1_MAX_REACH + (gintMinSpacing * (h + 2 - gintNumberOfHeads(A))))
                {
                    colLaneAllocationList[armNumber, headNumber].band = colAllocation[columnNumber].band;
                    colLaneAllocationList[armNumber, headNumber].allocated = true;

                    appAssignColumn[armNumber, headNumber] = columnNumber;
                    columnAssigned[columnNumber] = true;

                    intLastAllocatedHead[armNumber] = headNumber + 1; //zero is used as a check that nothing is assigned to the arm 

                    columnNumber++;
                }
                armNumber++;

            } while (armNumber < armParams.armCount && columnNumber < colAllocation.Count);

            int allocatedHeadSeperation = 0;
            int allocatedHeadGaps = 0;

            if (columnAssigned[colAllocation.Count - 1] == false)
            {
                do
                {
                    headNumber = 1;
                    do
                    {
                        armNumber = 0;
                        do
                        {
                            if (headNumber < armParams.headCounts[armNumber])
                            {
                                if (colLaneAllocationList[armNumber, headNumber].allocated == false)
                                {
                                    int requiredSpaceForArmEnd = armParams.armLength - ((armParams.headCounts[armNumber] - headNumber) * armParams.headCentreMin);
                                    int maxSpaceFromArmStart = (armParams.headFirstMax + (armParams.headCentreMax * (headNumber)));

                                    if (colAllocation[columnNumber].band < maxSpaceFromArmStart &&
                                         requiredSpaceForArmEnd > colAllocation[columnNumber].band) //If (gintRowPosition(r) + gintLabelCentreOffset) < (HEAD1_MAX_REACH + (gintMinSpacing * (h + 2 - gintNumberOfHeads(A))))
                                    {
                                        if (intLastAllocatedHead[armNumber] > 0)
                                        {
                                            allocatedHeadSeperation = colAllocation[columnNumber].band - colLaneAllocationList[armNumber, intLastAllocatedHead[armNumber] - 1].band;
                                            allocatedHeadGaps = headNumber - (intLastAllocatedHead[armNumber] - 1); //making them both start their count at zero
                                        }

                                        if (intLastAllocatedHead[armNumber] == 0 ||
                                            (allocatedHeadSeperation < (armParams.headCentreMax * allocatedHeadGaps) && allocatedHeadSeperation > (armParams.headCentreMin * allocatedHeadGaps)))
                                        {
                                            colLaneAllocationList[armNumber, headNumber].band = colAllocation[columnNumber].band;
                                            colLaneAllocationList[armNumber, headNumber].allocated = true;

                                            appAssignColumn[armNumber, headNumber] = columnNumber;
                                            columnAssigned[columnNumber] = true;

                                            intLastAllocatedHead[armNumber] = headNumber + 1; //zero is used as a check that nothing is assigned to the arm 
                                        }
                                    }
                                }
                            }

                            armNumber++;
                        } while (columnAssigned[columnNumber] == false && armNumber < armParams.armCount);
                        headNumber++;
                    } while (columnAssigned[columnNumber] == false && headNumber < maxHead);
                    columnNumber++;
                } while (columnNumber < colAllocation.Count);
            }

            for (int columnToRemove = colAllocation.Count -1; columnToRemove >= 0 ; columnToRemove--)
            {
                if (columnAssigned[columnToRemove] == false)
                {
                    removeColumnsNFruit(columnToRemove);
                }
            }

            int lastAllocationHeadNumber = 0;
            int firstAllocationHeadNumber = 0;

            for (armNumber = 0; armNumber < armParams.armCount; armNumber++)
            {
                lastAllocationHeadNumber = 0;
                firstAllocationHeadNumber = 0;

                for (int head = 0; head < armParams.headCounts[armNumber]; head++)
                {
                    if (firstAllocationHeadNumber == 0)
                    {
                        if (colLaneAllocationList[armNumber, head].allocated == true)
                        {
                            firstAllocationHeadNumber = head +1;
                        }
                    }
                }

                for (int head = armParams.headCounts[armNumber] - 1; head >= 0; head--)
                {
                    if (lastAllocationHeadNumber == 0)
                    {
                        if (colLaneAllocationList[armNumber, head].allocated == true)
                        {
                            lastAllocationHeadNumber = head +1;
                        }
                    }
                }

                if (firstAllocationHeadNumber > 0)
                {
                    for (int head = firstAllocationHeadNumber - 2; head >= 0; head--) //take it to the number for 1 below the first allocated FOR THE ARRAY POSITION
                    {
                        if (colLaneAllocationList[armNumber, head + 1].band - armParams.headCentreMin > armParams.headFirstMax + (head * armParams.headCentreMax))
                        {
                            colLaneAllocationList[armNumber, head].band = armParams.headFirstMax + (head * armParams.headCentreMax);
                            colLaneAllocationList[armNumber, head].allocated = true;
                        }
                        else
                        {
                            colLaneAllocationList[armNumber, head].band = colLaneAllocationList[armNumber, head + 1].band - armParams.headCentreMin;
                            colLaneAllocationList[armNumber, head].allocated = true;
                        }
                        
                    }
                }

                if (lastAllocationHeadNumber > 0)
                {
                    for (int head = lastAllocationHeadNumber; head < armParams.headCounts[armNumber]; head++) //lastallocation.. already takes it to the number for 1 above the last allocated FOR THE ARRAY POSITION
                    {
                        colLaneAllocationList[armNumber, head].band = colLaneAllocationList[armNumber, head - 1].band + armParams.headCentreMin;
                        colLaneAllocationList[armNumber, head].allocated = true;
                    }
                }

                if (lastAllocationHeadNumber == 0 && firstAllocationHeadNumber == 0)
                {
                    colLaneAllocationList[armNumber, 0].band = armParams.headCentreMin;
                    colLaneAllocationList[armNumber, 0].allocated = true;
                }

                for (int head = 1; head < armParams.headCounts[armNumber]; head++)
                {
                    if (colLaneAllocationList[armNumber, head].allocated == false)
                    {
                        colLaneAllocationList[armNumber, head].band = colLaneAllocationList[armNumber, head - 1].band + armParams.headCentreMin;
                        //colLaneAllocationList[armNumber, head].allocated = true;
                    }
                }

                for (int head = lastAllocationHeadNumber - 2; head > firstAllocationHeadNumber - 1; head--)
                {
                    if (colLaneAllocationList[armNumber, head].allocated == false)
                    {
                        if (colLaneAllocationList[armNumber, head + 1].band - colLaneAllocationList[armNumber, head].band > armParams.headCentreMax)
                        {
                            colLaneAllocationList[armNumber, head].band = colLaneAllocationList[armNumber, head + 1].band - armParams.headCentreMax;
                        }
                        colLaneAllocationList[armNumber, head].allocated = true;
                    }
                }

            }

            //for (int i = 0; i < colAllocation.Count; i++)
            //{
            //    if (colAllocation[i].allocated == false)
            //    {
            //        removeColumnsNFruit(i);
            //    }
            //}

            //return false;
        }

        public void TL4ApplicatorAllocationCheck()
        {
            for (int arm = 0; arm < armParams.armCount; arm++)
            {
                for (int head = 0; head < armParams.headCounts[arm]; head++)
                {

                }
            }
        }

        //######################################################################################


        int recursionCounter;
        int oldAlgorithmCount = 0;

        public allocErrorCode allocateSearch(ref TrayRecord.trayLayout tl, int maxPasses, ref List<string> report, TrayRecord.PositionType pt, ref int fruitTargets, bool forceZeroColBandWidth)
        {
            string report2 = "";
            incrementCounter++;

            //if (incrementCounter == 63/* || incrementCounter == 2 || incrementCounter == 10*/)
            //{
            //    ;
            //}

            allocErrorCode retError = allocErrorCode.NoError;
            try
            {

                if (forceZeroColBandWidth == true)
                {
                    armParams.columnBandWidth = 1;
                }

                //if (maxPasses < 1)
                //{
                //    report += "max Passes < 1";
                //    return allocErrorCode.PassRequestEqualsZero;
                //}

                /* Need to determine the column count based on all fruit, not just the unmasked. */
                faList = new List<fruitAllocation>();
                rowBands = new List<int>();
                colAllocation = new List<colBandAllocation>();

                getAllFruit(tl, armParams);
                determineBands(RowCol.Col, armParams);
                //DistanceBetweenBands();
                //removeExtraColumns();
                int evInd = colAllocation.Count & 0x1;
                tl.evenColumnCount = evInd == 0;

                faList = new List<fruitAllocation>();
                colAllocation = new List<colBandAllocation>();

                getUnmaskedFruit(tl, armParams, pt);

                determineBands(RowCol.Col, armParams);
                determineBands(RowCol.Row, armParams);


                // Antonio: polskie fix
                //fruitLocations();
                //logger.Debug(strFruitPositionString);

                //DistanceBetweenBands();
                //removeExtraColumns();

                int maxHead = getMaxHeadCount();
                headPosns = new headInfo[maxPasses, armParams.armCount, maxHead];

                TL4AllocationAlgorithm();

                //updateHeadMinMax(ref headPosns);
                updateHeadAllocation(ref headPosns);
                setUnusedHeads();
                SetAllocationToFruit();

                report.Add(textReport());
                return retError;

                // If in VRS mode and there are more columns than applicators,
                // then remove fruit that is in the latter columns
                if (tl.vrsMode == true)
                {
                    if (ColumnBandsToLanes())
                    {

                        updateHeadAllocation(ref headPosns);
                        SetUnusedHeadsToPrevious();
                        SetNewAllocationToFruit();
                        logger.Debug(debugString);
                        report.Add(textReport());

                        //for (int pass = 0; pass < headPosns.GetLength(0); pass++)
                        //{
                        for (int arm = 0; arm < armParams.armCount; arm++)
                        {
                            //rep += "\r\nPass " + pass.ToString() + ", Arm " + arm.ToString() + ": ";
                            for (int head = 0; head < armParams.headCounts[arm]; head++)
                            {
                                headPosition[arm, head] = headPosns[0, arm, head].actual;
                            }
                        }
                        //}
                        return retError;
                    }
                    oldAlgorithmCount++;
                    int maxColCount = 0;
                    for (int headCountIdx = 0; headCountIdx < armParams.armCount; headCountIdx++)
                    {
                        maxColCount += armParams.headCounts[headCountIdx];
                    }

                    if (colAllocation.Count > maxColCount)
                    {
                        retError = allocErrorCode.vrsColumnsReduced;
                        // Need to remove fruit from allocation
                        // First remove the column allocations
                        int maxColBand = colAllocation[maxColCount - 1].band;
                        colAllocation.RemoveRange(maxColCount, colAllocation.Count - maxColCount);

                        evInd = colAllocation.Count & 0x1;
                        tl.evenColumnCount = evInd == 0;

                        // Now remove fruit allocated to the deleted columns
                        List<int> faDeletions = new List<int>();
                        for (int faIdx = 0; faIdx < faList.Count; faIdx++)
                        {
                            if (faList[faIdx].colBand > maxColBand)
                            {
                                faDeletions.Add(faIdx);
                            }
                        }

                        faDeletions.Reverse();
                        foreach (int deletefa in faDeletions)
                        {
                            faList.RemoveAt(deletefa);
                        }

                        tl.reducedColumnCount = maxColCount;
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Debug(ex.Message);
                return allocErrorCode.SanityCheckFail; //just using this error for now
            }
            fruitTargets = faList.Count;

            // Calculate now if enough passes are available
            int maxColsPerPass = 0;
            for (int n = 0; n < armParams.armCount; n++)
            {
                maxColsPerPass += armParams.headCounts[n];
            }
            int minPassCount = (int)(Math.Ceiling((double)colAllocation.Count / maxColsPerPass));

            if (minPassCount > maxPasses)
            {
                //report += "minPassCount > maxPasses";
                return allocErrorCode.InsufficientPasses;
            }

            //int maxHead = getMaxHeadCount();
            //headPosns = new headInfo[maxPasses, armParams.armCount, maxHead];

            times[0] = Environment.TickCount;

            recursionCounter = 0;

            resetHeadMinMax(armParams, maxPasses, true);

            if (!sanityCheckPositions(tl, armParams, ref report2))
            {
                //report += "Sanity check failed";
                return allocErrorCode.SanityCheckFail;
            }

            // Need to preallocate memory for the head allocation
            // The size of the List is the same as the number of columns to
            // allocate as each recursion allocates one head.

            colAllocRecursive = new List<List<colBandAllocation>>(colAllocation.Count + 1);
            headPosnsRecursive = new List<headInfo[,,]>(colAllocation.Count + 1);

            for (int colIdx = 0; colIdx < colAllocation.Count + 1; colIdx++)
            {
                headInfo[,,] hiCopy = CopyDataHeadInfo(headPosns);
                headPosnsRecursive.Add(hiCopy);

                List<colBandAllocation> cbaCopy = CopyDataColBandAllocList(colAllocation);
                colAllocRecursive.Add(cbaCopy);
            }

            switch (DoRecursiveAllocation(0))
            {
                case recursionResult.AllocationComplete:
                    headPosns = CopyDataHeadInfo(headPosnsRecursive[0]);
                    //retError = allocErrorCode.NoError;
                    break;
                case recursionResult.NoResult:
                    retError = allocErrorCode.AllPathsSearched;
                    break;
            }

            times[1] = Environment.TickCount;
            times[2] = times[1] - times[0];

            tl.recursionCount = recursionCounter;

            updateHeadMinMax(ref headPosns);
            setUnusedHeads();
            SetAllocationToFruit();
            logger.Debug(debugString);
            report.Add(textReport());

            for (int pass = 0; pass < headPosns.GetLength(0); pass++)
            {
                for (int arm = 0; arm < armParams.armCount; arm++)
                {
                    //rep += "\r\nPass " + pass.ToString() + ", Arm " + arm.ToString() + ": ";
                    for (int head = 0; head < armParams.headCounts[arm]; head++)
                    {
                        headPosition[arm, head] = headPosns[pass, arm, head].actual;
                    }
                }
            }

            return retError;
        }

        private headInfo[,,] CopyDataHeadInfo(headInfo[,,] src)
        {
            headInfo[,,] dst = new headInfo[src.GetLength(0), src.GetLength(1), src.GetLength(2)];

            for (int a = 0; a < src.GetLength(0); a++)
            {
                for (int b = 0; b < src.GetLength(1); b++)
                {
                    for (int c = 0; c < src.GetLength(2); c++)
                    {
                        dst[a, b, c].actual = src[a, b, c].actual;
                        dst[a, b, c].max = src[a, b, c].max;
                        dst[a, b, c].min = src[a, b, c].min;
                        dst[a, b, c].used = src[a, b, c].used;
                    }
                }
            }
            return dst;
        }

        private List<colBandAllocation> CopyDataColBandAllocList(List<colBandAllocation> src)
        {
            List<colBandAllocation> cbaLis = new List<colBandAllocation>(src.Count);

            foreach (colBandAllocation cbaSrc in src)
            {
                colBandAllocation cba = new colBandAllocation();

                cba.allocated = cbaSrc.allocated;
                cba.arm = cbaSrc.arm;
                cba.band = cbaSrc.band;
                cba.head = cbaSrc.head;
                cba.pass = cbaSrc.pass;
                cba.possibles = cbaSrc.possibles;

                cbaLis.Add(cba);
            }
            return cbaLis;
        }

        private recursionResult DoRecursiveAllocation(int recurseIdx)
        {
            recursionResult result;

            recursionCounter++;
            updateHeadMinMaxRecursive(recurseIdx);
            FindPossiblesRecursive(recurseIdx);

            do
            {
                int notAllocated = 0;
                /* By here all columns may be allocated so check for number of unallocated */
                for (int colIdx = 0; colIdx < colAllocRecursive[recurseIdx].Count; colIdx++)
                {
                    if (colAllocRecursive[recurseIdx][colIdx].allocated == false)
                    {
                        notAllocated++;
                    }
                }

                if (notAllocated == 0)
                {
                    return recursionResult.AllocationComplete;
                }

                /* If any unallocated columns have no possibilities, then return with an error */
                for (int column = 0; column < colAllocRecursive[recurseIdx].Count; column++)
                {
                    if (colAllocRecursive[recurseIdx][column].allocated == false)
                    {
                        if (colAllocRecursive[recurseIdx][column].possibles.Count == 0)
                        {
                            debugString += "\r\npossibles - " + recurseIdx + " - Column " + column + "\r\n";
                            return recursionResult.NoResult;
                        }

                        int untested = 0;
                        for (int possIdx = 0; possIdx < colAllocRecursive[recurseIdx][column].possibles.Count; possIdx++)
                        {
                            if (colAllocRecursive[recurseIdx][column].possibles[possIdx].tested == false)
                            {
                                untested++;
                            }
                        }

                        if (untested == 0)
                        {
                            debugString += "\r\nuntested - " + recurseIdx + " - Column " + column + "\r\n";
                            return recursionResult.NoResult;
                        }
                    }
                }

                // Copy current head positions to next
                headInfo[,,] hiCopy = CopyDataHeadInfo(headPosnsRecursive[recurseIdx]);
                headPosnsRecursive.RemoveAt(recurseIdx + 1);
                headPosnsRecursive.Insert(recurseIdx + 1, hiCopy);

                // Copy current column allocations to next
                List<colBandAllocation> cbaLCopy = CopyDataColBandAllocList(colAllocRecursive[recurseIdx]);
                colAllocRecursive.RemoveAt(recurseIdx + 1);
                colAllocRecursive.Insert(recurseIdx + 1, cbaLCopy);

                Boolean updateAlloc = false;
                int possibleCount = 0;
                /* If any unallocated columns only have one possibility then assign the first found. */
                for (int column = 0; column < colAllocRecursive[recurseIdx].Count; column++)
                {
                    if (colAllocRecursive[recurseIdx][column].possibles.Count == 1)
                    {
                        possibleCount++;

                        if (colAllocRecursive[recurseIdx][column].possibles[0].tested == false)
                        {
                            // Set the tested flag
                            colBandAllocation cba = new colBandAllocation();

                            cba.allocated = colAllocRecursive[recurseIdx][column].allocated;
                            cba.pass = colAllocRecursive[recurseIdx][column].pass;
                            cba.arm = colAllocRecursive[recurseIdx][column].arm;
                            cba.head = colAllocRecursive[recurseIdx][column].head;
                            cba.band = colAllocRecursive[recurseIdx][column].band;

                            headReference hr = new headReference();

                            hr.pass = colAllocRecursive[recurseIdx][column].possibles[0].pass;
                            hr.arm = colAllocRecursive[recurseIdx][column].possibles[0].arm;
                            hr.head = colAllocRecursive[recurseIdx][column].possibles[0].head;
                            hr.col = colAllocRecursive[recurseIdx][column].possibles[0].col;
                            hr.tested = true;

                            cba.possibles = new List<headReference>();
                            cba.possibles.Add(hr);

                            colAllocRecursive[recurseIdx].RemoveAt(column);
                            colAllocRecursive[recurseIdx].Insert(column, cba);

                            if (updateAlloc == false)
                            {
                                // Now setup the allocation for the next recursion
                                int passIdx = colAllocRecursive[recurseIdx][column].possibles[0].pass;
                                int armIdx = colAllocRecursive[recurseIdx][column].possibles[0].arm;
                                int headIdx = colAllocRecursive[recurseIdx][column].possibles[0].head;

                                headPosnsRecursive[recurseIdx + 1][passIdx, armIdx, headIdx].actual = colAllocRecursive[recurseIdx][column].band;
                                headPosnsRecursive[recurseIdx + 1][passIdx, armIdx, headIdx].used = headStatus.allocated;

                                cba = colAllocRecursive[recurseIdx + 1][column];
                                cba.allocated = true;
                                colAllocRecursive[recurseIdx + 1].RemoveAt(column);
                                colAllocRecursive[recurseIdx + 1].Insert(column, cba);
                            }
                            updateAlloc = true;
                        }
                    }
                }

                if (possibleCount > 0)
                {
                    if (updateAlloc == false)
                    {
                        // No single possibility has been found that wasn't previously tested so go back
                        return recursionResult.NoResult;
                    }
                }

                if (!updateAlloc)
                {
                    /* Try the next available possibility */
                    if (FindNextRecPossibility(recurseIdx) == false)
                    {
                        // Possibilities have been exhausted, so return
                        return recursionResult.NoResult;
                    }
                }

                result = DoRecursiveAllocation(recurseIdx + 1);

                if (result == recursionResult.AllocationComplete)
                {
                    colAllocRecursive[recurseIdx] = colAllocRecursive[recurseIdx + 1];
                    headPosnsRecursive[recurseIdx] = headPosnsRecursive[recurseIdx + 1];

                    // Copy last head positions to current
                    hiCopy = CopyDataHeadInfo(headPosnsRecursive[recurseIdx + 1]);
                    headPosnsRecursive.RemoveAt(recurseIdx);
                    headPosnsRecursive.Insert(recurseIdx, hiCopy);

                    // Copy last column allocations to current
                    cbaLCopy = CopyDataColBandAllocList(colAllocRecursive[recurseIdx + 1]);
                    colAllocRecursive.RemoveAt(recurseIdx);
                    colAllocRecursive.Insert(recurseIdx, cbaLCopy);
                }
            } while (result == recursionResult.NoResult);

            return result;
        }

        private Boolean FindNextRecPossibility(int recurseIdx)
        {
            for (int cbaListIdx = 0; cbaListIdx < colAllocRecursive[recurseIdx].Count; cbaListIdx++)
            {
                headReference hr = new headReference();
                Boolean endNextRec = false;

                for (int possIndex = 0; possIndex < colAllocRecursive[recurseIdx][cbaListIdx].possibles.Count; possIndex++)
                {
                    if (colAllocRecursive[recurseIdx][cbaListIdx].possibles[possIndex].tested == false)
                    {
                        hr = colAllocRecursive[recurseIdx][cbaListIdx].possibles[possIndex];
                        hr.tested = true;

                        colAllocRecursive[recurseIdx][cbaListIdx].possibles.RemoveAt(possIndex);
                        colAllocRecursive[recurseIdx][cbaListIdx].possibles.Insert(possIndex, hr);

                        int pass = colAllocRecursive[recurseIdx][cbaListIdx].possibles[possIndex].pass;
                        int arm = colAllocRecursive[recurseIdx][cbaListIdx].possibles[possIndex].arm;
                        int head = colAllocRecursive[recurseIdx][cbaListIdx].possibles[possIndex].head;

                        colBandAllocation cba = colAllocRecursive[recurseIdx + 1][cbaListIdx];
                        cba.allocated = true;
                        cba.pass = pass;
                        cba.arm = arm;
                        cba.head = head;

                        colAllocRecursive[recurseIdx + 1].RemoveAt(cbaListIdx);
                        colAllocRecursive[recurseIdx + 1].Insert(cbaListIdx, cba);

                        headPosnsRecursive[recurseIdx + 1][pass, arm, head].actual = colAllocRecursive[recurseIdx][cbaListIdx].band;
                        headPosnsRecursive[recurseIdx + 1][pass, arm, head].used = headStatus.allocated;

                        endNextRec = true;
                        break;
                    }
                }

                if (endNextRec == true)
                {
                    //  A possibility has been found, but before returning, set any other symmetric possibilities
                    //  tested parameter to true.These don't need to be tested as they will yield the same result
                    //  as testing the first one.
                    for (int possIndex = 0; possIndex < colAllocRecursive[recurseIdx][cbaListIdx].possibles.Count; possIndex++)
                    {
                        if (colAllocRecursive[recurseIdx][cbaListIdx].possibles[possIndex].tested == false)
                        {
                            headReference hrCheck = colAllocRecursive[recurseIdx][cbaListIdx].possibles[possIndex];
                            if ((hr.pass == hrCheck.pass) && (hr.head == hrCheck.head))
                            {
                                hrCheck.tested = true;
                                colAllocRecursive[recurseIdx][cbaListIdx].possibles.RemoveAt(possIndex);
                                colAllocRecursive[recurseIdx][cbaListIdx].possibles.Insert(possIndex, hrCheck);
                            }
                        }
                    }

                    return true;
                }
            }
            return false;
        }

        private Boolean isAllAllocated()
        {
            Boolean allocated = true;
            foreach (colBandAllocation cba in colAllocation)
            {
                allocated &= cba.allocated;
            }
            return allocated;
        }

        private void SetAllocationToFruit()
        {
            for (int n = 0; n < faList.Count; n++)
            {
                headReference hr = new headReference();
                hr = SearchAllocatedColumn(faList[n].colBand);

                fruitAllocation fa = faList[n];
                fa.finalAllocation = hr;

                // Add the deltas dependant on the allocated arm
                fa.conveyorX = fa.rowBand;
                for (int m = 0; m <= hr.arm; m++)
                {
                    fa.conveyorX += armParams.armDeltas[m];
                }
                faList.RemoveAt(n);
                faList.Insert(n, fa);
            }
        }

        private headReference SearchAllocatedColumn(int col)
        {
            headReference pah = new headReference();

            for (int pass = 0; pass < headPosns.GetLength(0); pass++)
            {
                for (int arm = 0; arm < headPosns.GetLength(1); arm++)
                {
                    for (int head = 0; head < headPosns.GetLength(2); head++)
                    {
                        if (headPosns[pass, arm, head].used == headStatus.allocated)
                        {
                            if (headPosns[pass, arm, head].actual == col)
                            {
                                pah.pass = pass;
                                pah.arm = arm;
                                pah.head = head;
                                return pah;
                            }
                        }
                    }
                }
            }
            return pah;
        }

        private void FindPossiblesRecursive(int recIdx)
        {
            int pass;
            int arm;
            int head;

            List<colBandAllocation> colAllNew = new List<colBandAllocation>();

            int colIdx = 0;
            foreach (colBandAllocation cba in colAllocRecursive[recIdx])
            {
                colBandAllocation cb = cba;
                int colPos = cb.band;

                //debugString += "\r\nRecursive ID: " + recIdx;

                cb.possibles = new List<headReference>();

                if (cb.allocated == false)
                {
                    for (pass = 0; pass < headPosnsRecursive[recIdx].GetLength(0); pass++)
                    {
                        for (arm = 0; arm < headPosnsRecursive[recIdx].GetLength(1); arm++)
                        {
                            /* Find/fix all of the allocated head positions first */
                            for (head = 0; head < headPosnsRecursive[recIdx].GetLength(2); head++)
                            {
                                //debugString += "\r\ncolumn " + colIdx + " position : " + colPos +
                                //    "\r\nMax: " + headPosnsRecursive[recIdx][pass, arm, head].max +
                                //    "\r\nMin: " + headPosnsRecursive[recIdx][pass, arm, head].min;
                                if ((headPosnsRecursive[recIdx][pass, arm, head].max >= colPos) && (headPosnsRecursive[recIdx][pass, arm, head].min <= colPos))
                                {
                                    headReference hr = new headReference();
                                    hr.pass = pass;
                                    hr.arm = arm;
                                    hr.head = head;
                                    hr.col = colIdx;
                                    hr.tested = false;
                                    cb.possibles.Add(hr);
                                }
                            }
                        }
                    }
                }
                colAllNew.Add(cb);
                colIdx++;
            }
            colAllocRecursive[recIdx] = colAllNew;
        }

        private bool sanityCheckPositions(TrayRecord.trayLayout tl, ArmAllocatorParams armParams, ref string alRep)
        {
            bool ret = true;

            int minColPosition = armParams.headCentreMin / 2;
            int maxColPosition = armParams.armLength - (int)(armParams.headCentreMin / 2);

            foreach (colBandAllocation cba in colAllocation)
            {
                if (cba.band > maxColPosition)
                {
                    alRep += "Cannot allocate: Fruit column beyond arm length\r\n";
                    ret = false;
                }

                if (cba.band < minColPosition)
                {
                    alRep += "Cannot allocate: Fruit column < min position (" + cba.band.ToString() + " mm)\r\n";
                    ret = false;
                }
            }
            return (ret);
        }

        private void getUnmaskedFruit(TrayRecord.trayLayout tl, ArmAllocatorParams armParams, TrayRecord.PositionType pt)
        {
            foreach (TrayRecord.fruitPosition fp in tl.fruitsReal)
            {
                fruitAllocation fa = new fruitAllocation();

                if ((fp.masked == false) && (fp.posType == pt))
                {
                    fa.trayX = fp.x;
                    fa.trayY = fp.y;
                    // This is calculated from arm datum and conveyor offset parameters

                    if (armParams.applicatorDatumLeft)
                    {
                        switch (armParams.productAlignment)
                        {
                            case productAlign.Right:
                                fa.conveyorY = armParams.datumOffset - fp.y;
                                break;
                            case productAlign.Centre:
                                fa.conveyorY = armParams.datumOffset + ((tl.width / 2) - fp.y);
                                break;
                            case productAlign.Left:
                                fa.conveyorY = armParams.datumOffset + (tl.width - fp.y);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        switch (armParams.productAlignment)
                        {
                            case productAlign.Right:
                                fa.conveyorY = fp.y + armParams.datumOffset;
                                break;
                            case productAlign.Centre:
                                fa.conveyorY = fp.y + armParams.datumOffset - (tl.width / 2);
                                break;
                            case productAlign.Left:
                                fa.conveyorY = fp.y + armParams.datumOffset - tl.width;
                                break;
                            default:
                                break;
                        }
                    }

                    fa.colAllocated = false;
                    fa.rowAllocated = false;

                    faList.Add(fa);
                }

            }
        }

        private void getAllFruit(TrayRecord.trayLayout tl, ArmAllocatorParams armParams)
        {
            foreach (TrayRecord.fruitPosition fp in tl.fruitsReal)
            {
                fruitAllocation fa = new fruitAllocation();

                fa.trayX = fp.x;
                fa.trayY = fp.y;
                fa.conveyorY = fp.y;
                faList.Add(fa);
            }
        }

        private int getMaxHeadCount()
        {
            int maxHead = 0;
            for (int n = 0; n < armParams.headCounts.Length; n++)
            {
                if (armParams.headCounts[n] > maxHead)
                {
                    maxHead = armParams.headCounts[n];
                }
            }
            return (maxHead);
        }

        private void updateHeadMinMax(ref headInfo[,,] headPosns)
        {
            int pass;
            int arm;
            int head;

            for (pass = 0; pass < headPosns.GetLength(0); pass++)
            {
                for (arm = 0; arm < headPosns.GetLength(1); arm++)
                {
                    /* Find/fix all of the allocated head positions first */
                    for (head = 0; head < headPosns.GetLength(2); head++)
                    {
                        if (headPosns[pass, arm, head].used == headStatus.allocated)
                        {
                            headPosns[pass, arm, head].min = headPosns[pass, arm, head].actual;
                            headPosns[pass, arm, head].max = headPosns[pass, arm, head].actual;
                        }
                    }

                    /* Update min/max from allocated heads */
                    for (head = 0; head < headPosns.GetLength(2); head++)
                    {
                        if (headPosns[pass, arm, head].used == headStatus.allocated)
                        {
                            /* Working down the heads */
                            for (int headNum = head - 1; headNum >= 0; headNum--)
                            {
                                if (headPosns[pass, arm, headNum].used == headStatus.allocated)
                                {
                                    break;
                                }
                                else
                                {
                                    int newMax = headPosns[pass, arm, headNum + 1].max - armParams.headCentreMin;
                                    if (newMax < headPosns[pass, arm, headNum].max)
                                    {
                                        headPosns[pass, arm, headNum].max = newMax;
                                    }

                                    int newMin = headPosns[pass, arm, headNum + 1].min - armParams.headCentreMax;
                                    if (newMin > headPosns[pass, arm, headNum].min)
                                    {
                                        headPosns[pass, arm, headNum].min = newMin;
                                    }
                                }
                            }

                            /* Working up the heads */
                            for (int headNum = head + 1; headNum < headPosns.GetLength(2); headNum++)
                            {
                                if (headPosns[pass, arm, headNum].used == headStatus.allocated)
                                {
                                    break;
                                }
                                else
                                {
                                    int newMax = headPosns[pass, arm, headNum - 1].max + armParams.headCentreMax;
                                    if (newMax < headPosns[pass, arm, headNum].max)
                                    {
                                        headPosns[pass, arm, headNum].max = newMax;
                                    }

                                    int newMin = headPosns[pass, arm, headNum - 1].min + armParams.headCentreMin;
                                    if (newMin > headPosns[pass, arm, headNum].min)
                                    {
                                        headPosns[pass, arm, headNum].min = newMin;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void updateHeadMinMaxRecursive(int recurseIdx)
        {
            int pass;
            int arm;
            int head;

            for (pass = 0; pass < headPosnsRecursive[recurseIdx].GetLength(0); pass++)
            {
                for (arm = 0; arm < headPosnsRecursive[recurseIdx].GetLength(1); arm++)
                {
                    /* Find/fix all of the allocated head positions first */
                    for (head = 0; head < headPosnsRecursive[recurseIdx].GetLength(2); head++)
                    {
                        if (headPosnsRecursive[recurseIdx][pass, arm, head].used == headStatus.allocated)
                        {
                            headPosnsRecursive[recurseIdx][pass, arm, head].min = headPosnsRecursive[recurseIdx][pass, arm, head].actual;
                            headPosnsRecursive[recurseIdx][pass, arm, head].max = headPosnsRecursive[recurseIdx][pass, arm, head].actual;
                        }
                    }

                    /* Update min/max from allocated heads */
                    for (head = 0; head < headPosnsRecursive[recurseIdx].GetLength(2); head++)
                    {
                        if (headPosnsRecursive[recurseIdx][pass, arm, head].used == headStatus.allocated)
                        {
                            /* Working down the heads */
                            for (int headNum = head - 1; headNum >= 0; headNum--)
                            {
                                if (headPosnsRecursive[recurseIdx][pass, arm, headNum].used == headStatus.allocated)
                                {
                                    break;
                                }
                                else
                                {
                                    int newMax = headPosnsRecursive[recurseIdx][pass, arm, headNum + 1].max - armParams.headCentreMin;
                                    if (newMax < headPosnsRecursive[recurseIdx][pass, arm, headNum].max)
                                    {
                                        headPosnsRecursive[recurseIdx][pass, arm, headNum].max = newMax;
                                    }

                                    int newMin = headPosnsRecursive[recurseIdx][pass, arm, headNum + 1].min - armParams.headCentreMax;
                                    if (newMin > headPosnsRecursive[recurseIdx][pass, arm, headNum].min)
                                    {
                                        headPosnsRecursive[recurseIdx][pass, arm, headNum].min = newMin;
                                    }
                                }
                            }

                            /* Working up the heads */
                            for (int headNum = head + 1; headNum < headPosnsRecursive[recurseIdx].GetLength(2); headNum++)
                            {
                                if (headPosnsRecursive[recurseIdx][pass, arm, headNum].used == headStatus.allocated)
                                {
                                    break;
                                }
                                else
                                {
                                    int newMax = headPosnsRecursive[recurseIdx][pass, arm, headNum - 1].max + armParams.headCentreMax;
                                    if (newMax < headPosnsRecursive[recurseIdx][pass, arm, headNum].max)
                                    {
                                        headPosnsRecursive[recurseIdx][pass, arm, headNum].max = newMax;
                                    }

                                    int newMin = headPosnsRecursive[recurseIdx][pass, arm, headNum - 1].min + armParams.headCentreMin;
                                    if (newMin > headPosnsRecursive[recurseIdx][pass, arm, headNum].min)
                                    {
                                        headPosnsRecursive[recurseIdx][pass, arm, headNum].min = newMin;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void resetHeadMinMax(ArmAllocatorParams armParams, int maxPasses, Boolean setUnused)
        {
            for (int j = 0; j < maxPasses; j++)
            {
                for (int m = 0; m < armParams.armCount; m++)
                {
                    // Forward for min positions and initial max positions
                    for (int n = 0; n < armParams.headCounts[m]; n++)
                    {
                        if (n == 0)
                        {
                            headPosns[j, m, n].min = (int)Math.Ceiling((double)((armParams.headCentreMin) / 2));
                            headPosns[j, m, n].max = armParams.headFirstMax;
                        }
                        else
                        {
                            headPosns[j, m, n].min = headPosns[j, m, n - 1].min + armParams.headCentreMin;
                            headPosns[j, m, n].max = headPosns[j, m, n - 1].max + armParams.headCentreMax;
                        }

                        if (setUnused)
                        {
                            headPosns[j, m, n].used = headStatus.unused;
                        }
                    }

                    // Backwardward for max positions and consider the maximum arm length
                    for (int n = armParams.headCounts[m] - 1; n >= 0; n--)
                    {
                        if (n == armParams.headCounts[m] - 1)
                        {
                            headPosns[j, m, n].max = armParams.headFirstMax + ((armParams.headCounts[m] - 1) * armParams.headCentreMax);
                            if (headPosns[j, m, n].max > (armParams.armLength - (int)(armParams.headCentreMin / 2)))
                            {
                                headPosns[j, m, n].max = armParams.armLength - (int)(armParams.headCentreMin / 2);
                            }
                        }
                        else
                        {
                            int newMax = headPosns[j, m, n + 1].max - armParams.headCentreMin;
                            if (newMax < headPosns[j, m, n].max)
                            {
                                headPosns[j, m, n].max = newMax;
                            }
                        }
                    }
                }
            }
        }

        private void SetUnusedHeadsToPrevious()
        {
            for (int pass = 0; pass < headPosns.GetLength(0); pass++)
            {
                for (int arm = 0; arm < headPosns.GetLength(1); arm++)
                {
                    for (int head = 0; head < armParams.headCounts[arm]; head++)
                    {
                        //if (headPosns[pass, arm, head].used != headStatus.allocated)
                        //{
                        //    if (headPosition[arm, head] >= headPosns[pass, arm, head].min &&
                        //       headPosition[arm, head] <= headPosns[pass, arm, head].max)
                        //    {
                        //        headPosns[pass, arm, head].used = headStatus.allocated;
                        //        headPosns[pass, arm, head].actual = headPosition[arm, head];
                        //        headPosns[pass, arm, head].min = headPosition[arm, head];
                        //        headPosns[pass, arm, head].max = headPosition[arm, head];
                        //        AdjustRemainingMaxMin(pass, arm, head);
                        //    }
                        //    else if (headPosition[arm, head] < headPosns[pass, arm, head].min)
                        //    {
                        //        headPosns[pass, arm, head].used = headStatus.allocated;
                        //        headPosns[pass, arm, head].actual = headPosns[pass, arm, head].min;
                        //        headPosns[pass, arm, head].max = headPosns[pass, arm, head].min;
                        //        AdjustRemainingMaxMin(pass, arm, head);
                        //    }
                        //    else
                        //    {
                        //        headPosns[pass, arm, head].used = headStatus.allocated;
                        //        headPosns[pass, arm, head].actual = headPosns[pass, arm, head].max;
                        //        headPosns[pass, arm, head].min = headPosns[pass, arm, head].max;
                        //        AdjustRemainingMaxMin(pass, arm, head);
                        //    }
                        //}
                        if (headPosns[pass, arm, head].used != headStatus.allocated)
                        {
                            if (headPosition[arm, head] >= headPosns[pass, arm, head].min &&
                               headPosition[arm, head] <= headPosns[pass, arm, head].max)
                            {
                                headPosns[pass, arm, head].used = headStatus.allocated;
                                headPosns[pass, arm, head].actual = headPosition[arm, head];
                                headPosns[pass, arm, head].min = headPosition[arm, head];
                                headPosns[pass, arm, head].max = headPosition[arm, head];
                                AdjustRemainingMaxMin(pass, arm, head);
                            }
                            else if (headPosition[arm, head] < headPosns[pass, arm, head].min + 2)
                            {
                                headPosns[pass, arm, head].used = headStatus.allocated;
                                headPosns[pass, arm, head].actual = headPosns[pass, arm, head].min + 2;
                                headPosns[pass, arm, head].max = headPosns[pass, arm, head].min + 2;
                                headPosns[pass, arm, head].min = headPosns[pass, arm, head].min + 2;
                                AdjustRemainingMaxMin(pass, arm, head);
                            }
                            else if (headPosition[arm, head] < headPosns[pass, arm, head].min)
                            {
                                headPosns[pass, arm, head].used = headStatus.allocated;
                                headPosns[pass, arm, head].actual = headPosns[pass, arm, head].min;
                                headPosns[pass, arm, head].max = headPosns[pass, arm, head].min;
                                headPosns[pass, arm, head].min = headPosns[pass, arm, head].min;
                                AdjustRemainingMaxMin(pass, arm, head);
                            }
                            else if (headPosition[arm, head] > headPosns[pass, arm, head].max - 2)
                            {
                                headPosns[pass, arm, head].used = headStatus.allocated;
                                headPosns[pass, arm, head].actual = headPosns[pass, arm, head].max - 2;
                                headPosns[pass, arm, head].max = headPosns[pass, arm, head].max - 2;
                                headPosns[pass, arm, head].min = headPosns[pass, arm, head].max - 2;
                                AdjustRemainingMaxMin(pass, arm, head);
                            }
                            else
                            {
                                headPosns[pass, arm, head].used = headStatus.allocated;
                                headPosns[pass, arm, head].actual = headPosns[pass, arm, head].max;
                                headPosns[pass, arm, head].min = headPosns[pass, arm, head].max;
                                headPosns[pass, arm, head].max = headPosns[pass, arm, head].max;
                                AdjustRemainingMaxMin(pass, arm, head);
                            }
                        }
                    }
                }
            }
        }

        public void AdjustRemainingMaxMin(int pass, int arm, int head)
        {

            /* Update min/max from allocated heads */
            //for (/*head = 0*/; head < headPosns.GetLength(2); head++)
            //{
            //if (headPosns[pass, arm, head].used == headStatus.allocated)
            //{
            /* Working down the heads */
            for (int headNum = head - 1; headNum >= 0; headNum--)
            {
                if (headPosns[pass, arm, headNum].used == headStatus.allocated)
                {
                    break;
                }
                else
                {
                    int newMax = headPosns[pass, arm, headNum + 1].max - armParams.headCentreMin;
                    if (newMax < headPosns[pass, arm, headNum].max)
                    {
                        headPosns[pass, arm, headNum].max = newMax;
                    }

                    int newMin = headPosns[pass, arm, headNum + 1].min - armParams.headCentreMax;
                    if (newMin > headPosns[pass, arm, headNum].min)
                    {
                        headPosns[pass, arm, headNum].min = newMin;
                    }
                }
            }

            /* Working up the heads */
            for (int headNum = head + 1; headNum < armParams.headCounts[arm]; headNum++)
            {
                if (headPosns[pass, arm, headNum].used == headStatus.allocated)
                {
                    break;
                }
                else
                {
                    int newMax = headPosns[pass, arm, headNum - 1].max + armParams.headCentreMax;
                    if (newMax < headPosns[pass, arm, headNum].max)
                    {
                        headPosns[pass, arm, headNum].max = newMax;
                    }

                    int newMin = headPosns[pass, arm, headNum - 1].min + armParams.headCentreMin;
                    if (newMin > headPosns[pass, arm, headNum].min)
                    {
                        headPosns[pass, arm, headNum].min = newMin;
                    }
                }
            }

            //}
            //}
        }


        private void setUnusedHeads()
        {
            for (int pass = 0; pass < headPosns.GetLength(0); pass++)
            {
                for (int arm = 0; arm < headPosns.GetLength(1); arm++)
                {
                    for (int head = 0; head < headPosns.GetLength(2); head++)
                    {
                        if (headPosns[pass, arm, head].used != headStatus.allocated)
                        {
                            headPosns[pass, arm, head].actual = headPosns[pass, arm, head].min;
                        }
                    }
                }
            }
        }

        private string textReport()
        {
            string rep = "";
            //string rep = colAllocation.Count.ToString() + " column bands: ";
            //foreach (colBandAllocation colBand in colAllocation)
            //{
            //    rep += colBand.band.ToString("###") + ", ";
            //}

            //rep += "\r\n" + rowBands.Count.ToString() + " row bands: ";
            //foreach (int band in rowBands)
            //{
            //    rep += band.ToString("###") + ", ";
            //}

            //rep += "\r\nHead Allocation:\r\n";
            //rep += "\r\nBand  Pass  Arm  Head\r\n";
            //foreach (colBandAllocation colBand in colAllocation)
            //{
            //    rep += string.Format("{0,4}", colBand.band);
            //    //rep += string.Format("{0,5}", colBand.pass);
            //    //rep += string.Format("{0,5}", colBand.arm);
            //    //rep += string.Format("{0,5}", colBand.head);
            //    rep += "\r\n";
            //}

            //rep += "\r\nAll head positions";
            for (int pass = 0; pass < headPosns.GetLength(0); pass++)
            {
                for (int arm = 0; arm < headPosns.GetLength(1); arm++)
                {
                    //rep += "\r\nPass " + pass.ToString() + ", Arm " + arm.ToString() + ": ";
                    for (int head = 0; head < headPosns.GetLength(2); head++)
                    {
                        rep += headPosns[pass, arm, head].actual.ToString() + ", ";
                    }
                }
            }

            //rep += "\r\n\r\n RTray RBelt CTray CBelt ColBand RowBand\r\n";
            //foreach (fruitAllocation faRep in faList)
            //{
            //    rep += string.Format("{0,6},", faRep.trayX) +
            //        //string.Format("{0,6}", faRep.conveyorX) +
            //        string.Format("{0,6}", faRep.trayY) +
            //        //string.Format("{0,6}", faRep.conveyorY) +
            //        //string.Format("{0,8}", faRep.colBand) +
            //        //string.Format("{0,8}", faRep.rowBand) +
            //        "\r\n";
            //}
            logger.Debug(rep);
            //listboxResult
            return (rep);
        }

        private void determineBands(RowCol rc, ArmAllocatorParams armParams)
        {
            int fruitsToAllocate = faList.Count;

            int Min;
            int Max;

            while (fruitsToAllocate > 0)
            {
                Min = findMinXYUnallocated(rc);

                if (rc == RowCol.Col)
                {
                    Max = Min + armParams.columnBandWidth;
                }
                else
                {
                    Max = Min + armParams.rowBandWidth;
                }

                fruitsToAllocate -= allocateRowColumn(rc, Min, Max);
            }
        }

        private int findMinXYUnallocated(RowCol rc)
        {
            int min = 99999;

            foreach (fruitAllocation fa in faList)
            {
                switch (rc)
                {
                    case RowCol.Col:
                        if (fa.colAllocated == false)
                        {
                            if (fa.conveyorY < min)
                            {
                                min = fa.conveyorY;
                            }
                        }
                        break;
                    case RowCol.Row:
                        if (fa.rowAllocated == false)
                        {
                            if (fa.trayX < min)
                            {
                                min = fa.trayX;
                            }
                        }
                        break;
                }
            }
            return (min);
        }

        private int allocateRowColumn(RowCol rc, int Min, int Max)
        {
            int allocatedCount = 0;
            int average = 0;
            List<int> fruitIndices = new List<int>();

            colBandAllocation cb = new colBandAllocation();
            cb.fruitPositions = new List<int>();

            for (int n = 0; n < faList.Count; n++)
            {
                switch (rc)
                {
                    case RowCol.Col:
                        if (faList[n].colAllocated == false)
                        {
                            if ((faList[n].conveyorY >= Min) && (faList[n].conveyorY <= Max))
                            {
                                average += faList[n].conveyorY;
                                allocatedCount++;
                                fruitIndices.Add(n);
                                cb.fruitPositions.Add(faList[n].conveyorY);
                            }
                        }
                        break;
                    case RowCol.Row:
                        if (faList[n].rowAllocated == false)
                        {
                            if ((faList[n].trayX >= Min) && (faList[n].trayX <= Max))
                            {
                                average += faList[n].trayX;
                                allocatedCount++;
                                fruitIndices.Add(n);
                            }
                        }
                        break;
                }
            }

            average = (int)(Math.Round((float)average / allocatedCount));

            switch (rc)
            {
                case RowCol.Col:
                    cb.band = average;
                    cb.allocated = false;
                    colAllocation.Add(cb);
                    break;
                case RowCol.Row:
                    rowBands.Add(average);
                    break;
            }

            foreach (int index in fruitIndices)
            {
                fruitAllocation fa = new fruitAllocation();

                fa = faList[index];
                switch (rc)
                {
                    case RowCol.Col:
                        fa.colBand = average;
                        fa.colAllocated = true;
                        break;
                    case RowCol.Row:
                        fa.rowBand = average;
                        fa.rowAllocated = true;
                        break;
                }
                faList.RemoveAt(index);
                faList.Insert(index, fa);
            }
            return (allocatedCount);
        }

        private void CreateMachineConst(string line)
        {
            string[] lineElements = line.Split(';');
            string[] armParameters = lineElements[0].Split('=');
            if (armParameters[0] == "armParams.applicatorDatumLeft")
            {
                armParams.applicatorDatumLeft = Convert.ToBoolean(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.armCount")
            {
                armParams.armCount = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.armLength")
            {
                armParams.armLength = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.columnBandWidth")
            {
                armParams.columnBandWidth = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.datumOffset")
            {
                armParams.datumOffset = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.headCentreMax")
            {
                armParams.headCentreMax = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.headCentreMin")
            {
                armParams.headCentreMin = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.headCounts[0]")
            {
                armParams.headCounts = new int[4];
                armParams.headCounts[0] = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.headCounts[1]")
            {
                armParams.headCounts[1] = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.headCounts[2]")
            {
                armParams.headCounts[2] = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.headCounts[3]")
            {
                armParams.headCounts[3] = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.headFirstMax")
            {
                armParams.headFirstMax = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.productAlignment")
            {
                if (armParameters[1] == "LabellerAllocation.productAlign.Right")
                {
                    armParams.productAlignment = LabellerAllocation.productAlign.Right;

                }
                else if (armParameters[1] == "LabellerAllocation.productAlign.Centre")
                {
                    armParams.productAlignment = LabellerAllocation.productAlign.Centre;
                }
                else
                {
                    armParams.productAlignment = LabellerAllocation.productAlign.Left;
                }
            }
            else if (armParameters[0] == "armParams.reverseLabelling")
            {
                armParams.reverseLabelling = Convert.ToBoolean(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.rowBandWidth")
            {
                armParams.rowBandWidth = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.armDeltas[0]")
            {
                armParams.armDeltas = new int[4];
                armParams.armDeltas[0] = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.armDeltas[1]")
            {
                armParams.armDeltas[1] = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.armDeltas[2]")
            {
                armParams.armDeltas[2] = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.armDeltas[3]")
            {
                armParams.armDeltas[3] = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.RevBeltSpeed")
            {
                armParams.RevBeltSpeed = Convert.ToInt32(armParameters[1]);
            }
            else if (armParameters[0] == "armParams.reversingOvershoot")
            {
                armParams.reversingOvershoot = Convert.ToInt32(armParameters[1]);
            }
        }

        private void LoadAllocationParameters()
        {
            armParams = new LabellerAllocation.ArmAllocatorParams();

            armParams.applicatorDatumLeft = false;
            armParams.armCount = 2;
            armParams.armLength = 450;
            armParams.columnBandWidth = 25;
            armParams.datumOffset = 6;
            armParams.headCentreMax = 143;
            armParams.headCentreMin = 68;

            armParams.headCounts = new int[4];

            armParams.headCounts[0] = 4;
            armParams.headCounts[1] = 4;
            armParams.headCounts[2] = 0;
            armParams.headCounts[3] = 0;

            armParams.headFirstMax = 250;
            armParams.productAlignment = LabellerAllocation.productAlign.Right;

            armParams.reverseLabelling = false;
            armParams.rowBandWidth = 10;

            armParams.armDeltas = new int[4];
            armParams.armDeltas[0] = 165;
            armParams.armDeltas[1] = 500;
            armParams.armDeltas[2] = 500;
            armParams.armDeltas[3] = 500;

            armParams.RevBeltSpeed = 0;
            armParams.reversingOvershoot = 0;








            //armParams = new LabellerAllocation.ArmAllocatorParams();

            //ParameterEditor.paramFetch pf = ParameterEditor.GetParameterValue("Applicator Datum");
            //if (pf.type == typeof(ParameterEditor.Hand))
            //{
            //    armParams.applicatorDatumLeft = (pf.value == ParameterEditor.Hand.Left.ToString());
            //}

            //pf = ParameterEditor.GetParameterValue("Number of Arms");
            //if (pf.type == typeof(int))
            //{
            //    armParams.armCount = int.Parse(pf.value);
            //}

            //pf = ParameterEditor.GetParameterValue("Arm Length");
            //if (pf.type == typeof(int))
            //{
            //    armParams.armLength = int.Parse(pf.value);
            //}

            //pf = ParameterEditor.GetParameterValue("Column Band Width");
            //if (pf.type == typeof(int))
            //{
            //    armParams.columnBandWidth = int.Parse(pf.value);
            //}

            //pf = ParameterEditor.GetParameterValue("Datum Offset");
            //if (pf.type == typeof(int))
            //{
            //    armParams.datumOffset = int.Parse(pf.value);
            //}

            //pf = ParameterEditor.GetParameterValue("Max Distance Between Applicator Centres");
            //if (pf.type == typeof(int))
            //{
            //    armParams.headCentreMax = int.Parse(pf.value);
            //}

            //pf = ParameterEditor.GetParameterValue("Min Distance Between Applicator Centres");
            //if (pf.type == typeof(int))
            //{
            //    armParams.headCentreMin = int.Parse(pf.value);
            //}

            //armParams.headCounts = new int[4];

            //pf = ParameterEditor.GetParameterValue("Number of Applicators Arm 1");
            //if (pf.type == typeof(int))
            //{
            //    armParams.headCounts[0] = int.Parse(pf.value);
            //}
            //pf = ParameterEditor.GetParameterValue("Number of Applicators Arm 2");
            //if (pf.type == typeof(int))
            //{
            //    armParams.headCounts[1] = int.Parse(pf.value);
            //}
            //pf = ParameterEditor.GetParameterValue("Number of Applicators Arm 3");
            //if (pf.type == typeof(int))
            //{
            //    armParams.headCounts[2] = int.Parse(pf.value);
            //}
            //pf = ParameterEditor.GetParameterValue("Number of Applicators Arm 4");
            //if (pf.type == typeof(int))
            //{
            //    armParams.headCounts[3] = int.Parse(pf.value);
            //}

            //pf = ParameterEditor.GetParameterValue("First Head Maximum Position");
            //if (pf.type == typeof(int))
            //{
            //    armParams.headFirstMax = int.Parse(pf.value);
            //}

            //pf = ParameterEditor.GetParameterValue("Product Alignment");
            //if (pf.type == typeof(ParameterEditor.Alignment))
            //{
            //    if (LabellerAllocation.productAlign.Centre.ToString() == pf.value)
            //    {
            //        armParams.productAlignment = LabellerAllocation.productAlign.Centre;
            //    }
            //    else if (LabellerAllocation.productAlign.Left.ToString() == pf.value)
            //    {
            //        armParams.productAlignment = LabellerAllocation.productAlign.Left;
            //    }
            //    else
            //    {
            //        armParams.productAlignment = LabellerAllocation.productAlign.Right;
            //    }
            //}

            //pf = ParameterEditor.GetParameterValue("Label in Reverse");
            //if (pf.type == typeof(Boolean))
            //{
            //    armParams.reverseLabelling = (pf.value == Boolean.TrueString);
            //}

            //pf = ParameterEditor.GetParameterValue("Row Band Width");
            //if (pf.type == typeof(int))
            //{
            //    armParams.rowBandWidth = int.Parse(pf.value);
            //}

            //armParams.armDeltas = new int[4];
            //pf = ParameterEditor.GetParameterValue("Tray Scanner to First Arm");
            //if (pf.type == typeof(int))
            //{
            //    armParams.armDeltas[0] = int.Parse(pf.value);
            //}
            //pf = ParameterEditor.GetParameterValue("Distance Between Arms 1 and 2");
            //if (pf.type == typeof(int))
            //{
            //    armParams.armDeltas[1] = int.Parse(pf.value);
            //}
            //pf = ParameterEditor.GetParameterValue("Distance Between Arms 2 and 3");
            //if (pf.type == typeof(int))
            //{
            //    armParams.armDeltas[2] = int.Parse(pf.value);
            //}
            //pf = ParameterEditor.GetParameterValue("Distance Between Arms 3 and 4");
            //if (pf.type == typeof(int))
            //{
            //    armParams.armDeltas[3] = int.Parse(pf.value);
            //}

            //pf = ParameterEditor.GetParameterValue("Reversing Belt Speed");
            //if (pf.type == typeof(int))
            //{
            //    try
            //    {
            //        armParams.RevBeltSpeed = float.Parse(pf.value);
            //    }
            //    catch
            //    {
            //        armParams.RevBeltSpeed = 0.0f;
            //    }
            //}

            //pf = ParameterEditor.GetParameterValue("Reversing Overshoot");
            //if (pf.type == typeof(int))
            //{
            //    try
            //    {
            //        armParams.reversingOvershoot = int.Parse(pf.value);
            //    }
            //    catch
            //    {
            //        armParams.reversingOvershoot = 0;
            //    }
            //}


        }

        public void swapFirstLassPass()
        {
            int lastPass = headPosns.GetLength(0) - 1;

            //Firstly modify the head positions for first and last pass
            if (headPosns.GetLength(0) > 1)
            {
                headInfo[,] firstPassPosns = new headInfo[headPosns.GetLength(1), headPosns.GetLength(2)];

                // Save first pass posns
                for (int arm = 0; arm < headPosns.GetLength(1); arm++)
                {
                    for (int head = 0; head < headPosns.GetLength(2); head++)
                    {
                        firstPassPosns[arm, head] = headPosns[0, arm, head];
                    }
                }

                // Copy last to first and restore saved first to last
                for (int arm = 0; arm < headPosns.GetLength(1); arm++)
                {
                    for (int head = 0; head < headPosns.GetLength(2); head++)
                    {
                        headPosns[0, arm, head] = headPosns[lastPass, arm, head];
                        headPosns[lastPass, arm, head] = firstPassPosns[arm, head];
                    }
                }

                //Now modify the final allocation to swap the passes
                for (int n = 0; n < allocationResult.Count; n++)
                {
                    fruitAllocation fa = allocationResult[n];
                    if (fa.finalAllocation.pass == 0)
                    {
                        fa.finalAllocation.pass = lastPass;
                    }
                    else if (fa.finalAllocation.pass == lastPass)
                    {
                        fa.finalAllocation.pass = 0;
                    }
                    allocationResult.RemoveAt(n);
                    allocationResult.Insert(n, fa);
                }
            }
        }
    }
}

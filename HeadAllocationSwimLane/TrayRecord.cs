using System;
using System.Collections.Generic;
using System.Text;

namespace HeadAllocationSwimLane
{
    public class TrayRecord
    {
        public enum PositionType {Normal, Lead, Lag, Complex};

        public struct fruitPosition
        {
            public int x;
            public int y;
            public bool selected;
            public Boolean masked;
            public PositionType posType;
        }

        public struct trayLayout
        {
            public int id;
            public int length;
            public int width;
            public int frameHeight;
            public int firstCol;
            public int lastCol;
            public int colCount;
            public int firstRow;
            public int lastRow;
            public int rowCount;
            public List<fruitPosition> fruitsReal;
            public List<fruitPosition> fruitsImage;
            public int imageWidth;
            public int imageHeight;
            public float imageScaling;
            public string description;
            public string barcode;
            public PositionType trayType;
            public Boolean evenColumnCount;
            public int boxId;
            public int ghostTrayX;
            public int ghostTrayY;
            public int ghostTrayLength;
            public int ghostTrayWidth;
            public Boolean vrsMode;
            public int recursionCount;
            public int reducedColumnCount;
        }
    }
}

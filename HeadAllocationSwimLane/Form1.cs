﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeadAllocationSwimLane
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int[] TotalHeadTravel = { 0,0,0,0,0,
                                0,0,0,0};



        public DataTable ConvertListToDataTable(List<string> list)
        {
            DataTable table = new DataTable();
            int counter = 1;

            table.Columns.Add("ID");
            table.Columns.Add("Arm 1 - Head 1");
            table.Columns.Add("Arm 1 - Head 2");
            table.Columns.Add("Arm 1 - Head 3");
            table.Columns.Add("Arm 1 - Head 4");
            table.Columns.Add("Arm 1 - Head 5");

            table.Columns.Add("Arm 2 - Head 1");
            table.Columns.Add("Arm 2 - Head 2");
            table.Columns.Add("Arm 2 - Head 3");
            table.Columns.Add("Arm 2 - Head 4");

            //add a CSV builder in here for optional output

            foreach (string HeadLines in list)
            {
                string[] heads = HeadLines.Split(',');
                table.Rows.Add(counter++, heads[0], heads[1], heads[2], heads[3], heads[4], heads[5], heads[6], heads[7], heads[8]);

            }

            return table;
        }

        public DataTable ConvertListToTravelTable(List<string> list)
        {
            DataTable table = new DataTable();
            int counter = 0;

            table.Columns.Add("ID");
            table.Columns.Add("Arm 1 - Head 1");
            table.Columns.Add("Arm 1 - Head 2");
            table.Columns.Add("Arm 1 - Head 3");
            table.Columns.Add("Arm 1 - Head 4");
            table.Columns.Add("Arm 1 - Head 5");

            table.Columns.Add("Arm 2 - Head 1");
            table.Columns.Add("Arm 2 - Head 2");
            table.Columns.Add("Arm 2 - Head 3");
            table.Columns.Add("Arm 2 - Head 4");

            int[] HeadTravel = { 34,102,170,238, 306,
                                34,102,170,238};

            int[] PrevHeadPosition = { 34, 102, 170, 238, 306,
                                34,102,170,238};



            //add a CSV builder in here for optional output

            foreach (string HeadLines in list)
            {
                string[] heads = HeadLines.Split(',');
                //for loop thgrough heads
                for (int i = 0; i <= 8; i++)
                {
                    HeadTravel[i] = Math.Abs(PrevHeadPosition[i] - Convert.ToInt32(heads[i]));
                    PrevHeadPosition[i] = Convert.ToInt32(heads[i]);

                    TotalHeadTravel[i] += HeadTravel[i];
                }

                table.Rows.Add(counter + "-" + ++counter, HeadTravel[0], HeadTravel[1], HeadTravel[2], HeadTravel[3], HeadTravel[4], HeadTravel[5], HeadTravel[6], HeadTravel[7], HeadTravel[8]);

            }

            table.Rows.Add("Total", TotalHeadTravel[0], TotalHeadTravel[1], TotalHeadTravel[2], TotalHeadTravel[3], TotalHeadTravel[4], TotalHeadTravel[5], TotalHeadTravel[6], TotalHeadTravel[7], TotalHeadTravel[8]);

            return table;
        }

        private void btnRunTest_Click(object sender, EventArgs e)
        {
            Initialisation init = new Initialisation();

            List<string> results = new List<string>();

            results = init.runTest();

            //foreach(string item in results)
            //{
            //    MessageBox.Show("UI:" + item);
            //    listBoxResults.Items.Insert(0, item);
            //}

            DataTable allocationTable = ConvertListToDataTable(results);
            dataGridView.DataSource = null;
            dataGridView.DataSource = allocationTable;

            DataTable TravelTable = ConvertListToTravelTable(results);
            dataGridTravel.DataSource = null;
            dataGridTravel.DataSource = TravelTable;

        }
    }
}
